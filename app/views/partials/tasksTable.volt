{{ partial("partials/pages") }}

{%- macro printTableRow(task, isHead) %}
        <div class="uk-grid">
            <div>
                {{ task['n'] }}
            </div>
            <div class="uk-width-7-10 uk-width-medium-9-10">
                {% if isHead %}
                    <span>{{ task['name'] }}</span>
                {% else %}
                    <a href="/task{{ task['n'] }}" class="">
                        {{ task['name'] }}
                    </a>
                {% endif %}
            </div>
        </div>
{%- endmacro %}

<div class="uk-width-1-1 table">
    <div class="table__head">
        {{ printTableRow([
            'n': '№',
            'name': 'Задача'
        ], true) }}
    </div>
    <div class="table__main">
        {{ printTableRow([
            'n': 1,
            'name': 'Робота з однією таблицею. Вибірка всіх даних задовольняючих умову, та сортування за певним полем'
        ], false) }}
        {{ printTableRow([
            'n': 2,
            'name': 'Робота з однією таблицею. Вибірка всіх даних та робота з ними'
        ], false) }}
        {{ printTableRow([
            'n': 3,
            'name': 'Робота з однією таблицею. Вибірка всіх даних задовольняючих умову'
        ], false) }}
        {{ printTableRow([
            'n': 4,
            'name': 'Робота з однією таблицею. Вставка даних'
        ], false) }}
        {{ printTableRow([
            'n': 5,
            'name': 'Робота з декількома таблицями. Вибір одного запису з головної таблиці по ідентифікатору та приєднання залежних таблиць'
        ], false) }}
        {{ printTableRow([
            'n': 6,
            'name': 'Робота з декількома таблицями. Вибір записів з головної таблиці та приєднання залежних таблиць'
        ], false) }}
    </div>
</div>
<div class="uk-width-1-1 uk-margin-top uk-container-center pages">
    {{ printPages(1, 7, 1, '') }}
</div>