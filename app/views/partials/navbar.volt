<div class="">
    <nav class="uk-navbar">
        <div class="uk-container uk-container-center">
            <ul class="uk-navbar-nav">
                <li class="{{ navbar == 'list' ? 'uk-active' : '' }}">
                    <a href="/">Список задач</a>
                </li>
                <li class="{{ navbar == 'info' ? 'uk-active' : '' }}">
                    <a href="/info">Системна інформація</a>
                </li>
                <li class="{{ navbar == 'about' ? 'uk-active' : '' }}">
                    <a href="/about">Про дослідження</a>
                </li>
            </ul>
        </div>
    </nav>
</div>