{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-1-3">
                    <p class="uk-h3">Процесор</p>
                    <hr>
                    <p>
                        <strong>Модель процесора:</strong>
                        {{ cpuInfo['model name'] }}
                    </p>
                    <p>
                        <strong>Кількість ядер:</strong>
                        {{ cpuInfo['processor'] + 1 }}
                    </p>
                    <p>
                        <strong>Частота (MHz):</strong>
                        {{ cpuInfo['cpu MHz'] }}
                    </p>
                    <p>
                        <strong>Розмір кеша (КБ):</strong>
                        <?php echo explode(' ', $cpuInfo['cache size'])[0] ?>
                    </p>
                </div>
                <div class="uk-width-1-3">
                    <p class="uk-h3">Оперативна пам'ять</p>
                    <hr>
                    <p>
                        <strong>Загальний об'єм (КБ):</strong>
                        {{ memInfo['MemTotal'] }}
                    </p>
                    <p>
                        <strong>Вільно (КБ):</strong>
                        {{ memInfo['MemFree'] }}
                    </p>
                </div>
                <div class="uk-width-1-3">
                    <p class="uk-h3">MySql</p>
                    <hr>
                    <p>
                        <strong>Версія:</strong>
                        {{ mysqlInfo['version'] }}
                    </p>
                    <p>
                        <strong>query_cache_limit:</strong>
                        {{ mysqlInfo['query_cache_limit'] }}
                    </p>
                    <p>
                        <strong>query_cache_size:</strong>
                        {{ mysqlInfo['query_cache_size'] }}
                    </p>
                    <p>
                        <strong>profiling:</strong>
                        {{ mysqlInfo['profiling'] }}
                    </p>
                    <p>
                        <strong>profiling_history_size:</strong>
                        {{ mysqlInfo['profiling_history_size'] }}
                    </p>
                    <form action="/info/prepare" method="post">
                        <button class="uk-button" type="submit">Підготувати базу даних</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}