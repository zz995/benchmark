{% extends 'layouts/task.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    {{ super() }}
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task1';</script>
    {{ super() }}
{% endblock %}