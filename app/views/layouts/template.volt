<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

    {% block style %}
        {{ stylesheet_link('/css/components/uikit.almost-flat.min.css') }}
        {{ stylesheet_link('/css/components/progress.min.css') }}
        {{ stylesheet_link('/css/index.css') }}
    {% endblock %}

    {% block scriptTop %}
        {{ javascript_include("/js/components/jquery.min.js") }}
        {{ javascript_include("/js/components/uikit.min.js") }}
        {{ javascript_include('/js/index.js') }}
    {% endblock %}

    {{ tag.printTitle() }}
</head>
<body>
    {% block navbar %}
        {{ partial("partials/navbar") }}
    {% endblock %}

    <div class="main">
        {{ content() }}
        {{ flashSession.output() }}
        {% block content %}
        {% endblock %}
    </div>

    {{ partial("partials/footer") }}

    {% block scriptEnd %}
    {% endblock %}
</body>
</html>