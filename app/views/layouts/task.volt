<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

    {% block style %}
        {{ stylesheet_link('/css/components/uikit.almost-flat.min.css') }}
        {{ stylesheet_link('/css/components/progress.min.css') }}
        {{ stylesheet_link('/css/index.css') }}
    {% endblock %}

    {% block scriptTop %}
        {{ javascript_include("/js/components/jquery.min.js") }}
        {{ javascript_include("/js/components/uikit.min.js") }}
        {{ javascript_include('/js/index.js') }}
    {% endblock %}

    {{ tag.printTitle() }}
</head>
<body>
    {% block navbar %}
        {{ partial("partials/navbar") }}
    {% endblock %}

    <div class="main">
        {%- macro printItemCode(title, code) %}
            <div class="code">
                <strong class="code__title">{{ title }}</strong>
                <pre class="code__body">{{ code | e }}</pre>
            </div>
        {%- endmacro %}

        {%- macro printMetric(title, path) %}
            <div class="">
                <span class="uk-h3">{{ title }}</span>
            </div>
            <div>
                <img src="{{ path }}">
            </div>
        {%- endmacro %}

        <div class="uk-margin-large">
            <div class="uk-container uk-container-center container">
                <div class="uk-grid uk-mar">
                    <div class="uk-width-1-1">
                        <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                    </div>
                </div>

                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        {% if data['countRecord'] is not empty %}
                            <p>
                                <strong>Назва таблиці: </strong>
                                {% for tableName, countRecord in data['countRecord'] %}
                                    {{ tableName }} ({{ countRecord }}){{ loop.last ? '' : ', ' }}
                                {% endfor %}
                            </p>
                        {% endif %}
                        {{ printItemCode('SQL код стрворення таблиці', data['showTable']) }}
                        {{ printItemCode('Взаємодія з базою за допомогою SQL', data['code']['sql']) }}
                        {{ printItemCode('Створення збереженої процедури', data['code']['proc']['created']) }}
                        {% block afterStoreProcCode %}

                        {% endblock %}
                        {{ printItemCode('Взаємодія з базою за допомогою збереженої процедури', data['code']['proc']['code']) }}
                        {{ printItemCode('Взаємодія з базою за допомогою будівельника запитів', data['code']['builder']) }}
                        {{ printItemCode('Взаємодія з базою за допомогою ORM', data['code']['orm']) }}
                    </div>
                    <div class="uk-width-1-1 uk-margin-large-bottom uk-margin-large-top">
                        <span class="uk-h2">Метрики</span>
                        <hr>
                        <div class="uk-alert">
                        <span>
                            CYCLO (Cyclomatic Complexity) – цикломатична складність пакетів (на основі числа розгалужень в коді типу if, for, foreach)
                        </span><br>
                            <span>
                            LOC (Lines Of Code) – число рядків коду
                        </span><br>
                            <span>
                            NOM (Number Of Methods+functions) – число методів класів + число функцій
                        </span><br>
                            <span>
                            NOC (Number Of Classes) – число класів
                        </span><br>
                            <span>
                            NOP (Number Of Packages) – число пакетів
                        </span><br>
                            <span>
                            AHH (Average Hierarchy Height) – середня глибина ієрархії
                        </span><br>
                            <span>
                            AND (Average Number of Derived classes) – середнє число класів-нащадків
                        </span><br>
                            <span>
                            FANOUT (Number of Called Classes) – число використань класів
                        </span><br>
                            <span>
                            CALLS (Number of Operation Calls) – число викликів методів і функцій
                        </span><br>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-2 uk-margin-top">
                                {{ printMetric('Взаємодія з базою за допомогою SQL', data['metrics']['sql']['pyramid']) }}
                            </div>
                            <div class="uk-width-1-2 uk-margin-top">
                                {{ printMetric('Взаємодія з базою за допомогою збереженої процедури', data['metrics']['proc']['pyramid']) }}
                            </div>
                            <div class="uk-width-1-2 uk-margin-large-top">
                                {{ printMetric('Взаємодія з базою за допомогою будівельника запитів', data['metrics']['builder']['pyramid']) }}
                            </div>
                            <div class="uk-width-1-2 uk-margin-large-top">
                                {{ printMetric('Взаємодія з базою за допомогою ORM', data['metrics']['orm']['pyramid']) }}
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-text-center">
                        <hr>
                        {% block link %}
                        {% endblock %}
                        <form class="uk-form">
                            {% block param %}
                                <span class="uk-display-inline-block uk-margin-top">
                                    <nobr class="uk-margin-left">
                                        <label for="count">Кількість повторень</label>
                                        <input id="count" name="count" type="number" value="1" min="1" max="1000">
                                    </nobr>
                                </span>
                            {% endblock %}
                            <div class="uk-margin-top">
                                <button id="send" type="button" class="uk-button uk-button-primary">
                                    Виконати
                                </button>
                            </div>
                        </form>
                    </div>

                    <div class="result-load uk-hidden uk-width-1-1 uk-text-center uk-margin-large-top uk-margin-bottom">
                        <div class="uk-progress">
                            <div class="uk-progress-bar sql-progress" style="width: 0%;"></div>
                        </div>
                        <div class="uk-progress">
                            <div class="uk-progress-bar proc-progress" style="width: 0%;"></div>
                        </div>
                        <div class="uk-progress">
                            <div class="uk-progress-bar builder-progress" style="width: 0%;"></div>
                        </div>
                        <div class="uk-progress">
                            <div class="uk-progress-bar orm-progress" style="width: 0%;"></div>
                        </div>
                    </div>

                    <div class="result uk-hidden uk-width-1-1 uk-margin-large-top"></div>
                </div>
            </div>
        </div>
    </div>

    {{ partial("partials/footer") }}

    {% block scriptEnd %}
        {{ javascript_include('/js/components/Chart.bundle.min.js') }}
        {{ javascript_include('/js/index.js') }}

        {{ javascript_include('/js/classes/LoadData.js') }}
        {{ javascript_include('/js/classes/LoadDataQuery.js') }}
        {{ javascript_include('/js/classes/Measures.js') }}
        {{ javascript_include('/js/classes/MeasuresHtml.js') }}

        {{ javascript_include('/js/task.js') }}
    {% endblock %}
</body>
</html>