{% extends 'layouts/task.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block link %}
    <div class="uk-margin-bottom">
        <a href="/task5/common" class="">
            <i class="uk-icon-list"></i>
            Загальна інформація
        </a>
    </div>
{% endblock %}

{% block param %}
    {{ super() }}
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="table">Кількість таблиць</label>
            <input id="table" name="table" type="number" value="1" min="1" max="15">
        </nobr>
    </span>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="records">Кількість записів</label>
            <select id="records" name="records">
                <option value="1">1</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="500">500</option>
            </select>
        </nobr>
    </span>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task5';</script>
    {{ super() }}
    {{ javascript_include('/js/task5.js') }}
{% endblock %}