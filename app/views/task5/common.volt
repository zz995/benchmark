{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-1-1 uk-text-center">
                    <hr>
                    <form class="uk-form">
                        {% for param in params %}
                            <div class="params">
                                <div class="uk-text-left uk-display-inline-block params__title">
                                    <span>{{ param['title'] }}:</span>
                                </div>
                                <ul class="uk-subnav uk-subnav-pill uk-display-inline-block">
                                    {% for index, value in param['values'] %}
                                        <li
                                                class="uk-active param"
                                                data-name="{{ param['name'] | e }}"
                                                data-value="{{ value | e }}"
                                                data-index="{{ index | e }}"
                                        >
                                            <a href="#">
                                                {{ value }}
                                            </a>
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        {% endfor %}

                        <span class="uk-display-inline-block uk-margin-top">
                            <nobr class="uk-margin-left">
                                <label for="count">Кількість повторень</label>
                                <input id="count" name="count" type="number" value="1" min="1" max="1000">
                            </nobr>
                        </span>
                        <div class="uk-margin-top">
                            <button id="send" type="button" class="uk-button uk-button-primary">
                                Виконати
                            </button>
                        </div>
                    </form>
                </div>

                <div class="result-load uk-hidden uk-width-1-1 uk-text-center uk-margin-large-top uk-margin-bottom">
                    <div class="uk-progress">
                        <div class="uk-progress-bar progress" style="width: 0%;"></div>
                    </div>
                </div>

                <div class="result uk-hidden uk-width-1-1 uk-margin-large-top"></div>
            </div>
        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task5';</script>
    {{ super() }}
    {{ javascript_include('/js/components/Chart.bundle.min.js') }}
    {{ javascript_include('/js/classes/LoadData.js') }}
    {{ javascript_include('/js/classes/LoadDataBase.js') }}
    {{ javascript_include('/js/classes/LoadDataRegression.js') }}
    {{ javascript_include('/js/classes/CommonMeasures.js') }}
    {{ javascript_include('/js/classes/CommonMeasuresHtml.js') }}
    {{ javascript_include('/js/task5_common.js') }}
{% endblock %}