{% extends 'layouts/task.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    {{ super() }}
{% endblock %}

{% block param %}
    {{ super() }}
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="records_count">Кількість записів</label>
            <input id="records_count" name="records_count" type="number" value="1" min="1" max="1000">
        </nobr>
    </span>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task4';</script>
    {{ super() }}
    {{ javascript_include('/js/task4.js') }}
{% endblock %}