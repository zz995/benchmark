{% extends 'layouts/task.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block link %}
    <div class="uk-margin-bottom">
        <a href="/task6/common" class="">
            <i class="uk-icon-list"></i>
            Загальна інформація
        </a>
    </div>
{% endblock %}

{% block param %}
    {{ super() }}
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="tablesCount">Кількість таблиць</label>
            <select id="tablesCount" name="tables_count">
                {% for c in 1 .. 15 %}
                    <option value="{{ c }}">{{ c }}</option>
                {% endfor %}
            </select>
        </nobr>
    </span>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="recordsMain">Кількість записів</label>
            <select id="recordsMain" name="records_main">
                <option value="1">1</option>
                <option value="3">3</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="75">75</option>
                <option value="100">100</option>
                <option value="125">125</option>
                <option value="150">150</option>
                <option value="175">175</option>
                <option value="200">200</option>
            </select>
        </nobr>
    </span>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="recordsSecond">Кількість записів у залежних таблицях</label>
            <select id="recordsSecond" name="records_second">
                {% for c in 0 .. 15 %}
                    <option value="{{ c }}">{{ c }}</option>
                {% endfor %}
            </select>
        </nobr>
    </span>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task6';</script>
    {{ super() }}
    {{ javascript_include('/js/task6.js') }}
{% endblock %}