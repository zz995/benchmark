{% extends 'layouts/task.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}
    {{ super() }}
{% endblock %}

{% block afterStoreProcCode %}
    {{ printItemCode('Створення збереженої функції', data['code']['func']['created']) }}
    {{ printItemCode('Створення визначеної користувачем функції', data['code']['udf']['created']) }}
{% endblock %}

{% block param %}
    {{ super() }}
    <br>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="sql_type">SQL</label>
            <select id="sql_type" name="sql_type">
                <option disabled>Перевірка умови</option>
                <option value="1">Функція</option>
                <option value="2">Частина запиту</option>
            </select>
        </nobr>
    </span>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="proc_type">Збережена процедура</label>
            <select id="proc_type" name="proc_type">
                <option disabled>Перевірка умови</option>
                <option value="1">Збережена функція</option>
                <option value="2">Ввизначена користувачем функція</option>
                <option value="3">Частина запиту</option>
            </select>
        </nobr>
    </span>
    <br>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="builder_type">Будівальник запитів</label>
            <select id="builder_type" name="builder_type">
                <option disabled>Перевірка умови</option>
                <option value="1">Функція</option>
                <option value="2">Частина запиту</option>
            </select>
        </nobr>
    </span>
    <span class="uk-display-inline-block uk-margin-top">
        <nobr class="uk-margin-left">
            <label for="orm_type">ORM</label>
            <select id="orm_type" name="orm_type">
                <option disabled>Перевірка умови</option>
                <option value="1">Функція</option>
                <option value="2">Частина запиту</option>
            </select>
        </nobr>
    </span>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}

{% block scriptEnd %}
    <script>window.TASK = 'task3';</script>
    {{ super() }}
    {{ javascript_include('/js/task3.js') }}
{% endblock %}