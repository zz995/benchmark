{% extends 'layouts/template.volt' %}

{% block navbar %}
    {{ super() }}
{% endblock %}

{% block content %}

    <div class="uk-margin-large">
        <div class="uk-container uk-container-center container">
            <div class="uk-grid uk-mar">
                <div class="uk-width-1-1">
                    <h1 class="uk-h1">{{ getTitle(false) }}</h1>
                </div>
            </div>
            <div class="uk-grid">
                Дослідження способів взаємодії з базою даних
            </div>
        </div>
    </div>
{% endblock %}

{% block style %}
    {{ super() }}
{% endblock %}