<?php

namespace MyApp\Models\Task5\Table13\Proc;

class ProcModel {
    static public function exec($id) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc5_table1_t13(?);";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch();

        $tables = ['table2', 'table3', 'table4', 'table5', 'table6', 'table7', 'table8', 'table9', 'table10', 'table11', 'table12', 'table13', 'table14'];
        $indexTable = 0;
        for(;$stmt->nextRowset() && $stmt->rowCount();) {
            $data[$tables[$indexTable++]] = $stmt->fetchAll();
        }
        
        return $data;
    }
}