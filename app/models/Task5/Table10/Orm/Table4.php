<?php

namespace MyApp\Models\Task5\Table10\Orm;

use \Phalcon\Mvc\Model;

class Table4 extends Model
{
    public $id;
    public $field1;

    public function initialize() {
        $this->setSource('sc5_table4');

        $this->belongsTo(
            "sc5_table1_id",
            'MyApp\Models\Task5\Table10\Orm\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}