<?php

namespace MyApp\Models\Task5\Table15\Orm;

use \Phalcon\Mvc\Model;

class Table1 extends Model
{
    public $id;
    public $field1;
    public $field2;

    public function initialize() {
        $this->setSource('sc5_table1');
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table2',
            "sc5_table1_id",
            array(
                'alias' => 'table2'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table3',
            "sc5_table1_id",
            array(
                'alias' => 'table3'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table4',
            "sc5_table1_id",
            array(
                'alias' => 'table4'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table5',
            "sc5_table1_id",
            array(
                'alias' => 'table5'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table6',
            "sc5_table1_id",
            array(
                'alias' => 'table6'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table7',
            "sc5_table1_id",
            array(
                'alias' => 'table7'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table8',
            "sc5_table1_id",
            array(
                'alias' => 'table8'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table9',
            "sc5_table1_id",
            array(
                'alias' => 'table9'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table10',
            "sc5_table1_id",
            array(
                'alias' => 'table10'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table11',
            "sc5_table1_id",
            array(
                'alias' => 'table11'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table12',
            "sc5_table1_id",
            array(
                'alias' => 'table12'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table13',
            "sc5_table1_id",
            array(
                'alias' => 'table13'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table14',
            "sc5_table1_id",
            array(
                'alias' => 'table14'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table15',
            "sc5_table1_id",
            array(
                'alias' => 'table15'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table15\Orm\Table16',
            "sc5_table1_id",
            array(
                'alias' => 'table16'
            )
        );

    }

    static public function exec($id) {
        $table1 = self::findFirst($id);
        $data = $table1->toArray();
        $data['table2'] = $table1->getTable2()->toArray();
        $data['table3'] = $table1->getTable3()->toArray();
        $data['table4'] = $table1->getTable4()->toArray();
        $data['table5'] = $table1->getTable5()->toArray();
        $data['table6'] = $table1->getTable6()->toArray();
        $data['table7'] = $table1->getTable7()->toArray();
        $data['table8'] = $table1->getTable8()->toArray();
        $data['table9'] = $table1->getTable9()->toArray();
        $data['table10'] = $table1->getTable10()->toArray();
        $data['table11'] = $table1->getTable11()->toArray();
        $data['table12'] = $table1->getTable12()->toArray();
        $data['table13'] = $table1->getTable13()->toArray();
        $data['table14'] = $table1->getTable14()->toArray();
        $data['table15'] = $table1->getTable15()->toArray();
        $data['table16'] = $table1->getTable16()->toArray();

        return $data;
    }
}