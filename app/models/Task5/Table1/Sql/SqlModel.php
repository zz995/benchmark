<?php

namespace MyApp\Models\Task5\Table1\Sql;

class SqlModel {
    static public function exec($id) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $data = $db->query("SELECT * FROM sc5_table1 WHERE id = " . intval($id))->fetch();
        $data['table2'] = $db->query("SELECT id, field1 FROM sc5_table2 WHERE sc5_table1_id = " . intval($id))->fetchAll();

        return $data;
    }
}