<?php

namespace MyApp\Models\Task5\Table11\Orm;

use \Phalcon\Mvc\Model;

class Table10 extends Model
{
    public $id;
    public $field1;

    public function initialize() {
        $this->setSource('sc5_table10');

        $this->belongsTo(
            "sc5_table1_id",
            'MyApp\Models\Task5\Table11\Orm\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}