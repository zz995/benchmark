<?php

namespace MyApp\Models\Task5\Table12\Sql;

class SqlModel {
    static public function exec($id) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $data = $db->query("SELECT * FROM sc5_table1 WHERE id = " . intval($id))->fetch();
        $data['table2'] = $db->query("SELECT id, field1 FROM sc5_table2 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table3'] = $db->query("SELECT id, field1 FROM sc5_table3 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table4'] = $db->query("SELECT id, field1 FROM sc5_table4 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table5'] = $db->query("SELECT id, field1 FROM sc5_table5 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table6'] = $db->query("SELECT id, field1 FROM sc5_table6 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table7'] = $db->query("SELECT id, field1 FROM sc5_table7 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table8'] = $db->query("SELECT id, field1 FROM sc5_table8 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table9'] = $db->query("SELECT id, field1 FROM sc5_table9 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table10'] = $db->query("SELECT id, field1 FROM sc5_table10 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table11'] = $db->query("SELECT id, field1 FROM sc5_table11 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table12'] = $db->query("SELECT id, field1 FROM sc5_table12 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table13'] = $db->query("SELECT id, field1 FROM sc5_table13 WHERE sc5_table1_id = " . intval($id))->fetchAll();

        return $data;
    }
}