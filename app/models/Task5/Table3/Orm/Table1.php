<?php

namespace MyApp\Models\Task5\Table3\Orm;

use \Phalcon\Mvc\Model;

class Table1 extends Model
{
    public $id;
    public $field1;
    public $field2;

    public function initialize() {
        $this->setSource('sc5_table1');
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table3\Orm\Table2',
            "sc5_table1_id",
            array(
                'alias' => 'table2'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table3\Orm\Table3',
            "sc5_table1_id",
            array(
                'alias' => 'table3'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task5\Table3\Orm\Table4',
            "sc5_table1_id",
            array(
                'alias' => 'table4'
            )
        );

    }

    static public function exec($id) {
        $table1 = self::findFirst($id);
        $data = $table1->toArray();
        $data['table2'] = $table1->getTable2()->toArray();
        $data['table3'] = $table1->getTable3()->toArray();
        $data['table4'] = $table1->getTable4()->toArray();

        return $data;
    }
}