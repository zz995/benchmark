<?php

namespace MyApp\Models\Task5\Table7\Orm;

use \Phalcon\Mvc\Model;

class Table7 extends Model
{
    public $id;
    public $field1;

    public function initialize() {
        $this->setSource('sc5_table7');

        $this->belongsTo(
            "sc5_table1_id",
            'MyApp\Models\Task5\Table7\Orm\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}