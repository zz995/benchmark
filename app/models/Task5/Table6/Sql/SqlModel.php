<?php

namespace MyApp\Models\Task5\Table6\Sql;

class SqlModel {
    static public function exec($id) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $data = $db->query("SELECT * FROM sc5_table1 WHERE id = " . intval($id))->fetch();
        $data['table2'] = $db->query("SELECT id, field1 FROM sc5_table2 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table3'] = $db->query("SELECT id, field1 FROM sc5_table3 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table4'] = $db->query("SELECT id, field1 FROM sc5_table4 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table5'] = $db->query("SELECT id, field1 FROM sc5_table5 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table6'] = $db->query("SELECT id, field1 FROM sc5_table6 WHERE sc5_table1_id = " . intval($id))->fetchAll();
        $data['table7'] = $db->query("SELECT id, field1 FROM sc5_table7 WHERE sc5_table1_id = " . intval($id))->fetchAll();

        return $data;
    }
}