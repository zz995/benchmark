<?php

namespace MyApp\Models\Task5\Table14\Orm;

use \Phalcon\Mvc\Model;

class Table14 extends Model
{
    public $id;
    public $field1;

    public function initialize() {
        $this->setSource('sc5_table14');

        $this->belongsTo(
            "sc5_table1_id",
            'MyApp\Models\Task5\Table14\Orm\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}