<?php
namespace MyApp\Models\Task2\Builder;
use MyApp\Library\QueryBuilder;
class BuilderModel {
    static public function exec() {
        $rows = QueryBuilder::table('sc2_table1')->get();

        $result = 0;
        foreach ($rows as $row) {
            if ($result > 1000) {
                $k = 0.8;
            } else {
                $k = 1;
            }
            $result += $row->field2 * $k;
        }

        return $result;
    }
}