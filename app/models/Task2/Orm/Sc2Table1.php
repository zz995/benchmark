<?php
namespace MyApp\Models\Task2\Orm;
use \Phalcon\Mvc\Model;
class Sc2Table1 extends Model {
    public $field1;
    public $field2;
    public function initialize() {
        $this->setSource('sc2_table1');
    }
    static public function exec() {
        $rows = self::find();

        $result = 0;
        foreach ($rows as $row) {
            if ($result > 1000) {
                $k = 0.8;
            } else {
                $k = 1;
            }
            $result += $row->field2 * $k;
        }

        return $result;
    }
}