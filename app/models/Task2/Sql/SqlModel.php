<?php
namespace MyApp\Models\Task2\Sql;
class SqlModel {
    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "SELECT * FROM sc2_table1";
        $rows = $db->query($sql, [])->fetchAll();

        $result = 0;
        foreach ($rows as $row) {
            if ($result > 1000) {
                $k = 0.8;
            } else {
                $k = 1;
            }
            $result += $row['field2'] * $k;
        }

        return $result;
    }
}