<?php
namespace MyApp\Models\Task2\Proc;
class ProcModel {
    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc2_table1();";
        $result = $db->query($sql, [])->fetch()['result'];
        return $result;
    }
}