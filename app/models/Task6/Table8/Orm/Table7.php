<?php

namespace MyApp\Models\Task6\Table8\Orm;

use \Phalcon\Mvc\Model;

class Table7 extends Model
{
    public $id;
    public $field1;

    public function initialize() {
        $this->setSource('sc6_table7');

        $this->belongsTo(
            "sc6_table1_id",
            'MyApp\Models\Task6\Table8\Orm\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}