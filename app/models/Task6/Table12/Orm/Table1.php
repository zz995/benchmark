<?php

namespace MyApp\Models\Task6\Table12\Orm;

use \Phalcon\Mvc\Model;

class Table1 extends Model
{
    public $id;
    public $field1;
    public $field2;

    public function initialize() {
        $this->setSource('sc6_table1');
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table2',
            "sc6_table1_id",
            array(
                'alias' => 'table2'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table3',
            "sc6_table1_id",
            array(
                'alias' => 'table3'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table4',
            "sc6_table1_id",
            array(
                'alias' => 'table4'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table5',
            "sc6_table1_id",
            array(
                'alias' => 'table5'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table6',
            "sc6_table1_id",
            array(
                'alias' => 'table6'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table7',
            "sc6_table1_id",
            array(
                'alias' => 'table7'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table8',
            "sc6_table1_id",
            array(
                'alias' => 'table8'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table9',
            "sc6_table1_id",
            array(
                'alias' => 'table9'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table10',
            "sc6_table1_id",
            array(
                'alias' => 'table10'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table11',
            "sc6_table1_id",
            array(
                'alias' => 'table11'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table12',
            "sc6_table1_id",
            array(
                'alias' => 'table12'
            )
        );
        $this->hasMany(
            "id",
            'MyApp\Models\Task6\Table12\Orm\Table13',
            "sc6_table1_id",
            array(
                'alias' => 'table13'
            )
        );

    }

    static public function exec($start, $end) {
        $tables = self::find(
            [
                'conditions' => 'id between :start: AND :end:',
                'bind' => [
                    'start' => $start,
                    'end' => $end,
                ],
            ]
        );
        
        $data = [];
        foreach($tables as $table) {
            $table1 = $table->toArray();
            $table1['table2'] = $table->getTable2()->toArray();
            $table1['table3'] = $table->getTable3()->toArray();
            $table1['table4'] = $table->getTable4()->toArray();
            $table1['table5'] = $table->getTable5()->toArray();
            $table1['table6'] = $table->getTable6()->toArray();
            $table1['table7'] = $table->getTable7()->toArray();
            $table1['table8'] = $table->getTable8()->toArray();
            $table1['table9'] = $table->getTable9()->toArray();
            $table1['table10'] = $table->getTable10()->toArray();
            $table1['table11'] = $table->getTable11()->toArray();
            $table1['table12'] = $table->getTable12()->toArray();
            $table1['table13'] = $table->getTable13()->toArray();

            $data[] = $table1;
        }

        return $data;
    }
}