<?php

namespace MyApp\Models\Task6\Table5\Sql;

class SqlModel {
    static public function exec($start, $end) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $data = $db->query("SELECT * FROM sc6_table1 WHERE id BETWEEN " . intval($start) . " AND " . intval($end))->fetchAll();
        $ids = array_map(function ($v) { return $v['id']; }, $data);

        $foreign = [];
        $foreign['table2'] = $db->query("SELECT * FROM sc6_table2 WHERE sc6_table1_id IN (" . implode(', ', $ids) . ")")->fetchAll();
        $foreign['table3'] = $db->query("SELECT * FROM sc6_table3 WHERE sc6_table1_id IN (" . implode(', ', $ids) . ")")->fetchAll();
        $foreign['table4'] = $db->query("SELECT * FROM sc6_table4 WHERE sc6_table1_id IN (" . implode(', ', $ids) . ")")->fetchAll();
        $foreign['table5'] = $db->query("SELECT * FROM sc6_table5 WHERE sc6_table1_id IN (" . implode(', ', $ids) . ")")->fetchAll();
        $foreign['table6'] = $db->query("SELECT * FROM sc6_table6 WHERE sc6_table1_id IN (" . implode(', ', $ids) . ")")->fetchAll();


        for ($i = 0; $i < count($data); $i++) {
            $id = $data[$i]['id'];

            foreach ($foreign as $key => $values) {
                if (!isset($data[$i][$key])) {
                    $data[$i][$key] = [];
                }

                foreach($values as $value) {
                    if ($value['sc6_table1_id'] == $id) {
                        $data[$i][$key][] = ['id' => $value['id'], 'field1' => $value['field1']];
                    }
                }
            }
        }
        return $data;
    }
}