<?php

namespace MyApp\Models\Task6\Table3\Proc;

class ProcModel {
    static public function exec($start, $end) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc6_table1_t3(?, ?);";
        $rows = $db->query($sql, [$start, $end])->fetchAll();

        $mData = [];
        $data = [];
        foreach ($rows as $row) {
            $id = $row['t1_id'];
            if (!isset($mData[$id])) {
                $mData[$id] = ['table1' => [], 'table2' => [], 'table3' => [], 'table4' => []];
                $mData[$id]['table1'] = [
                    'id' => $row['t1_id'],
                    'field1' => $row['t1_field1'],
                    'field2' => $row['t1_field2'],
                    'table2' => [], 'table3' => [], 'table4' => []
                ];
                $data[] = &$mData[$id]['table1'];
            }

            $t2Id = $row['t2_id'];
            if (!isset($mData[$id]['table2'][$t2Id]) && isset($t2Id)) {
                $mData[$id]['table2'][$t2Id] = 1;
                $mData[$id]['table1']['table2'][] = [
                    'id' => $row['t2_id'],
                    'field1' => $row['t2_field1']
                ];
            }
            $t3Id = $row['t3_id'];
            if (!isset($mData[$id]['table3'][$t3Id]) && isset($t3Id)) {
                $mData[$id]['table3'][$t3Id] = 1;
                $mData[$id]['table1']['table3'][] = [
                    'id' => $row['t3_id'],
                    'field1' => $row['t3_field1']
                ];
            }
            $t4Id = $row['t4_id'];
            if (!isset($mData[$id]['table4'][$t4Id]) && isset($t4Id)) {
                $mData[$id]['table4'][$t4Id] = 1;
                $mData[$id]['table1']['table4'][] = [
                    'id' => $row['t4_id'],
                    'field1' => $row['t4_field1']
                ];
            }

        }

        return $data;
    }
}