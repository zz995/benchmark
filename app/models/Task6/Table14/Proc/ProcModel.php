<?php

namespace MyApp\Models\Task6\Table14\Proc;

class ProcModel {
    static public function exec($start, $end) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc6_table1_t14(?, ?);";
        $rows = $db->query($sql, [$start, $end])->fetchAll();

        $mData = [];
        $data = [];
        foreach ($rows as $row) {
            $id = $row['t1_id'];
            if (!isset($mData[$id])) {
                $mData[$id] = ['table1' => [], 'table2' => [], 'table3' => [], 'table4' => [], 'table5' => [], 'table6' => [], 'table7' => [], 'table8' => [], 'table9' => [], 'table10' => [], 'table11' => [], 'table12' => [], 'table13' => [], 'table14' => [], 'table15' => []];
                $mData[$id]['table1'] = [
                    'id' => $row['t1_id'],
                    'field1' => $row['t1_field1'],
                    'field2' => $row['t1_field2'],
                    'table2' => [], 'table3' => [], 'table4' => [], 'table5' => [], 'table6' => [], 'table7' => [], 'table8' => [], 'table9' => [], 'table10' => [], 'table11' => [], 'table12' => [], 'table13' => [], 'table14' => [], 'table15' => []
                ];
                $data[] = &$mData[$id]['table1'];
            }

            $t2Id = $row['t2_id'];
            if (!isset($mData[$id]['table2'][$t2Id]) && isset($t2Id)) {
                $mData[$id]['table2'][$t2Id] = 1;
                $mData[$id]['table1']['table2'][] = [
                    'id' => $row['t2_id'],
                    'field1' => $row['t2_field1']
                ];
            }
            $t3Id = $row['t3_id'];
            if (!isset($mData[$id]['table3'][$t3Id]) && isset($t3Id)) {
                $mData[$id]['table3'][$t3Id] = 1;
                $mData[$id]['table1']['table3'][] = [
                    'id' => $row['t3_id'],
                    'field1' => $row['t3_field1']
                ];
            }
            $t4Id = $row['t4_id'];
            if (!isset($mData[$id]['table4'][$t4Id]) && isset($t4Id)) {
                $mData[$id]['table4'][$t4Id] = 1;
                $mData[$id]['table1']['table4'][] = [
                    'id' => $row['t4_id'],
                    'field1' => $row['t4_field1']
                ];
            }
            $t5Id = $row['t5_id'];
            if (!isset($mData[$id]['table5'][$t5Id]) && isset($t5Id)) {
                $mData[$id]['table5'][$t5Id] = 1;
                $mData[$id]['table1']['table5'][] = [
                    'id' => $row['t5_id'],
                    'field1' => $row['t5_field1']
                ];
            }
            $t6Id = $row['t6_id'];
            if (!isset($mData[$id]['table6'][$t6Id]) && isset($t6Id)) {
                $mData[$id]['table6'][$t6Id] = 1;
                $mData[$id]['table1']['table6'][] = [
                    'id' => $row['t6_id'],
                    'field1' => $row['t6_field1']
                ];
            }
            $t7Id = $row['t7_id'];
            if (!isset($mData[$id]['table7'][$t7Id]) && isset($t7Id)) {
                $mData[$id]['table7'][$t7Id] = 1;
                $mData[$id]['table1']['table7'][] = [
                    'id' => $row['t7_id'],
                    'field1' => $row['t7_field1']
                ];
            }
            $t8Id = $row['t8_id'];
            if (!isset($mData[$id]['table8'][$t8Id]) && isset($t8Id)) {
                $mData[$id]['table8'][$t8Id] = 1;
                $mData[$id]['table1']['table8'][] = [
                    'id' => $row['t8_id'],
                    'field1' => $row['t8_field1']
                ];
            }
            $t9Id = $row['t9_id'];
            if (!isset($mData[$id]['table9'][$t9Id]) && isset($t9Id)) {
                $mData[$id]['table9'][$t9Id] = 1;
                $mData[$id]['table1']['table9'][] = [
                    'id' => $row['t9_id'],
                    'field1' => $row['t9_field1']
                ];
            }
            $t10Id = $row['t10_id'];
            if (!isset($mData[$id]['table10'][$t10Id]) && isset($t10Id)) {
                $mData[$id]['table10'][$t10Id] = 1;
                $mData[$id]['table1']['table10'][] = [
                    'id' => $row['t10_id'],
                    'field1' => $row['t10_field1']
                ];
            }
            $t11Id = $row['t11_id'];
            if (!isset($mData[$id]['table11'][$t11Id]) && isset($t11Id)) {
                $mData[$id]['table11'][$t11Id] = 1;
                $mData[$id]['table1']['table11'][] = [
                    'id' => $row['t11_id'],
                    'field1' => $row['t11_field1']
                ];
            }
            $t12Id = $row['t12_id'];
            if (!isset($mData[$id]['table12'][$t12Id]) && isset($t12Id)) {
                $mData[$id]['table12'][$t12Id] = 1;
                $mData[$id]['table1']['table12'][] = [
                    'id' => $row['t12_id'],
                    'field1' => $row['t12_field1']
                ];
            }
            $t13Id = $row['t13_id'];
            if (!isset($mData[$id]['table13'][$t13Id]) && isset($t13Id)) {
                $mData[$id]['table13'][$t13Id] = 1;
                $mData[$id]['table1']['table13'][] = [
                    'id' => $row['t13_id'],
                    'field1' => $row['t13_field1']
                ];
            }
            $t14Id = $row['t14_id'];
            if (!isset($mData[$id]['table14'][$t14Id]) && isset($t14Id)) {
                $mData[$id]['table14'][$t14Id] = 1;
                $mData[$id]['table1']['table14'][] = [
                    'id' => $row['t14_id'],
                    'field1' => $row['t14_field1']
                ];
            }
            $t15Id = $row['t15_id'];
            if (!isset($mData[$id]['table15'][$t15Id]) && isset($t15Id)) {
                $mData[$id]['table15'][$t15Id] = 1;
                $mData[$id]['table1']['table15'][] = [
                    'id' => $row['t15_id'],
                    'field1' => $row['t15_field1']
                ];
            }

        }

        return $data;
    }
}