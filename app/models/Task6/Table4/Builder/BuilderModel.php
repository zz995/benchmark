<?php

namespace MyApp\Models\Task6\Table4\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec($start, $end) {
        $data = QueryBuilder::table('sc6_table1')->whereBetween('id', [$start, $end])->get()->toArray();
        $ids = array_map(function ($v) { return $v->id; }, $data);
        
        $foreign = []; 
        $foreign['table2'] = QueryBuilder::table('sc6_table2')->whereIn('sc6_table1_id', $ids)->get()->toArray();
        $foreign['table3'] = QueryBuilder::table('sc6_table3')->whereIn('sc6_table1_id', $ids)->get()->toArray();
        $foreign['table4'] = QueryBuilder::table('sc6_table4')->whereIn('sc6_table1_id', $ids)->get()->toArray();
        $foreign['table5'] = QueryBuilder::table('sc6_table5')->whereIn('sc6_table1_id', $ids)->get()->toArray();


        for ($i = 0; $i < count($data); $i++) {
            $id = $data[$i]->id;

            foreach ($foreign as $key => $values) {
                if (!isset($data[$i]->{$key})) {
                    $data[$i]->{$key} = [];
                }

                foreach($values as $value) {
                    if ($value->sc6_table1_id == $id) {
                        $data[$i]->{$key}[] = ['id' => $value->id, 'field1' => $value->field1];
                    }
                }
            }
        }
        return $data;
    }
}