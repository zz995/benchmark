<?php

namespace MyApp\Models\Task6\Table1\Proc;

class ProcModel {
    static public function exec($start, $end) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc6_table1_t1(?, ?);";
        $rows = $db->query($sql, [$start, $end])->fetchAll();

        $mData = [];
        $data = [];
        foreach ($rows as $row) {
            $id = $row['t1_id'];
            if (!isset($mData[$id])) {
                $mData[$id] = ['table1' => [], 'table2' => []];
                $mData[$id]['table1'] = [
                    'id' => $row['t1_id'],
                    'field1' => $row['t1_field1'],
                    'field2' => $row['t1_field2'],
                    'table2' => []
                ];
                $data[] = &$mData[$id]['table1'];
            }

            $t2Id = $row['t2_id'];
            if (!isset($mData[$id]['table2'][$t2Id]) && isset($t2Id)) {
                $mData[$id]['table2'][$t2Id] = 1;
                $mData[$id]['table1']['table2'][] = [
                    'id' => $row['t2_id'],
                    'field1' => $row['t2_field1']
                ];
            }

        }

        return $data;
    }
}