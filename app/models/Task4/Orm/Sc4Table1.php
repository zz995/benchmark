<?php

namespace MyApp\Models\Task4\Orm;

use \Phalcon\Mvc\Model;

class Sc4Table1 extends Model {

    public $field1;

    public function initialize() {
        $this->setSource('sc4_table1_orm');
    }

    static public function exec($records) {
        for ($i = 0; $i < $records; $i++) {
            $table = new self();
            $table->id = rand(10001, 999999999);
            $table->field1 = rand(1, 100);
            $table->save();
        }
    }
}