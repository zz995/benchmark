<?php

namespace MyApp\Models\Task4\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec($records) {
        $values = [];
        for ($i = 0; $i < $records; $i++) {
            $values[] = [
                'id' => rand(10001, 999999999),
                'field1' => rand(1, 100)
            ];
        };

        QueryBuilder::table('sc4_table1_builder')->insert($values);
    }
}