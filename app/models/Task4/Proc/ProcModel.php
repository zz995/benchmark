<?php

namespace MyApp\Models\Task4\Proc;

class ProcModel {
    static public function exec($records) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc4_table1(?);";
        $db->query($sql, [$records]);
    }
}