<?php

namespace MyApp\Models\Task4\Sql;

class SqlModel {
    static public function exec($records) {
        $db = \Phalcon\Di::getDefault()->getDb();

        $sql = "INSERT INTO sc4_table1_sql(`id`, `field1`) VALUES ";

        for ($i = 0; $i < $records; $i++) {
            $sql .= "(" . rand(10001, 999999999) . ',' . rand(1, 100) . ")";
            if (($i + 1) != $records) {
                $sql .= ',';
            }
        };

        $db->query($sql);
    }
}