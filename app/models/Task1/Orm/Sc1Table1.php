<?php
namespace MyApp\Models\Task1\Orm;
use \Phalcon\Mvc\Model;
class Sc1Table1 extends Model {
    public $field1;
    public $field2;
    public $field3;
    public function initialize() {
        $this->setSource('sc1_table1');
    }
    static public function exec($field1 = null, $field2 = null, $field3 = null, $sort) {
        $query = self::query();
        $bindParam = [];
        if (isset($field1)) {
            $query = $query->andWhere('field1 = :field1:');
            $bindParam['field1'] = $field1;
        }
        if (isset($field2)) {
            $query = $query->andWhere('field2 > :field2:');
            $bindParam['field2'] = $field2;
        }
        if (isset($field3)) {
            $query = $query->andWhere('field3 < :field3:');
            $bindParam['field3'] = $field3;
        }
        $query->bind($bindParam);
        if (in_array($sort, ['field2_asc', 'field2_desc', 'field3_asc', 'field3_desc'])) {
            $query = $query->orderBy(implode(' ', explode('_', $sort)) . ', id');
        } else {
            $query = $query->orderBy('id');
        }
        return json_encode($query->execute());
    }
}