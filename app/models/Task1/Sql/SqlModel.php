<?php
namespace MyApp\Models\Task1\Sql;
class SqlModel {
    static public function exec($field1 = null, $field2 = null, $field3 = null, $sort) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "SELECT * FROM sc1_table1";
        $bindParam = [];
        $conditions = '';

        if (isset($field1)) {
            $conditions .= 'field1 = :field1';
            $bindParam[':field1'] = $field1;
        }
        if (isset($field2)) {
            if ($conditions != '') {
                $conditions .= ' AND ';
            }
            $conditions .= 'field2 > :field2';
            $bindParam[':field2'] = $field2;
        }
        if (isset($field3)) {
            if ($conditions != '') {
                $conditions .= ' AND ';
            }
            $conditions .= 'field3 < :field3';
            $bindParam[':field3'] = $field3;
        }
        if ($conditions != '') {
            $sql .= ' WHERE ' . $conditions;
        }
        if (in_array($sort, ['field2_asc', 'field2_desc', 'field3_asc', 'field3_desc'])) {
            $sql .= ' ORDER BY ' . implode(' ', explode('_', $sort)) . ', id';
        } else {
            $sql = ' ORDER BY id';
        }
        $sth = $db->prepare($sql);
        $sth->execute($bindParam);
        return json_encode($sth->fetchAll());
    }
}