<?php
namespace MyApp\Models\Task1\Proc;
class ProcModel {
    static public function exec($field1 = null, $field2 = null, $field3 = null, $sort) {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc1_table1(?, ?, ?, ?);";
        return json_encode($db->query($sql, [$field1, $field2, $field3, $sort])->fetchAll());
    }
}