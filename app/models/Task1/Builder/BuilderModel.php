<?php
namespace MyApp\Models\Task1\Builder;
use MyApp\Library\QueryBuilder;
class BuilderModel {
    static public function exec($field1 = null, $field2 = null, $field3 = null, $sort) {
        $builder = QueryBuilder::table('sc1_table1');
        if (isset($field1)) {
            $builder = $builder->where('field1', $field1);
        }
        if (isset($field2)) {
            $builder = $builder->where('field2', '>', $field2);
        }
        if (isset($field3)) {
            $builder = $builder->where('field3', '<', $field3);
        }
        if (in_array($sort, ['field2_asc', 'field2_desc', 'field3_asc', 'field3_desc'])) {
            $builder = $builder->orderBy(...explode('_', $sort));
        }
        return json_encode($builder->orderBy('id', 'asc')->get());
    }
}