<?php

namespace MyApp\Models\Task3\Builder\Inline;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function partWhere(\Illuminate\Database\Query\Builder $builder): \Illuminate\Database\Query\Builder {
        $field1 = 10;
        $field2 = 30;
        $field3 = 50;

        $builder = $builder->where('field1', $field1);
        $builder = $builder->where('field2', '>', $field2);
        $builder = $builder->where('field3', '<', $field3);

        return $builder;
    }

    static public function exec() {
        $builder = QueryBuilder::table('sc3_table1');
        $builder = self::partWhere($builder);
        $data = $builder->get();

        return $data;
    }
}