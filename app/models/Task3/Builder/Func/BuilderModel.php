<?php

namespace MyApp\Models\Task3\Builder\Func;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function func($field1, $field2, $field3) {
        return ($field1 == 10 && $field2 > 30 && $field3 < 50);
    }

    static public function exec() {
        $rows = QueryBuilder::table('sc3_table1')->get();

        $data = [];
        foreach ($rows as $row) {
            $correct = self::func($row->field1, $row->field2, $row->field3);
            if ($correct) {
                $data[] = $row;
            }
        }

        return $data;
    }
}