<?php

namespace MyApp\Models\Task3\Orm\Func;

use \Phalcon\Mvc\Model;

class Sc3Table1 extends Model {

    public $field1;
    public $field2;
    public $field3;

    public function initialize() {
        $this->setSource('sc3_table1');
    }

    static public function func($field1, $field2, $field3) {
        return ($field1 == 10 && $field2 > 30 && $field3 < 50);
    }

    static public function exec() {
        $rows = self::find();

        $data = [];
        foreach ($rows as $row) {
            $correct = self::func($row->field1, $row->field2, $row->field3);
            if ($correct) {
                $data[] = $row;
            }
        }

        return $data;
    }
}