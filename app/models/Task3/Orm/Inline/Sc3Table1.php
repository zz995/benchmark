<?php

namespace MyApp\Models\Task3\Orm\Inline;

use \Phalcon\Mvc\Model;

class Sc3Table1 extends Model {

    public $field1;
    public $field2;
    public $field3;

    public function initialize() {
        $this->setSource('sc3_table1');
    }

    static public function partWhere(\Phalcon\Mvc\Model\Criteria $query): \Phalcon\Mvc\Model\Criteria {
        $field1 = 10;
        $field2 = 30;
        $field3 = 50;

        $query = $query->andWhere("field1 = $field1");
        $query = $query->andWhere("field2 > $field2");
        $query = $query->andWhere("field3 < $field3");

        return $query;
    }

    static public function exec() {
        $query = self::query();
        $query = self::partWhere($query);
        $data = $query->execute();

        return $data;
    }
}