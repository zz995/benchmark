<?php

namespace MyApp\Models\Task3\Sql\Inline;

class SqlModel {
    static public function getPartWhere() {
        $field1 = 10;
        $field2 = 30;
        $field3 = 50;

        return "field1 = $field1 AND field2 > $field2 AND field3 < $field3";
    }

    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "SELECT * FROM sc3_table1 WHERE " . self::getPartWhere();
        $data = $db->query($sql, [])->fetchAll();

        return $data;
    }
}