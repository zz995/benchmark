<?php

namespace MyApp\Models\Task3\Proc\Func;

class ProcModel {
    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc3_table1_stored_function();";
        $data = $db->query($sql, [])->fetchAll();
        return $data;
    }
}