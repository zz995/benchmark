<?php

namespace MyApp\Models\Task3\Proc\Inline;

class ProcModel {
    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc3_table1_inline();";
        $data = $db->query($sql, [])->fetchAll();
        return $data;
    }
}