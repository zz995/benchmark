<?php

namespace MyApp\Models\Task3\Proc\UFunc;

class ProcModel {
    static public function exec() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $sql = "CALL sc3_table1_udf();";
        $data = $db->query($sql, [])->fetchAll();
        return $data;
    }
}