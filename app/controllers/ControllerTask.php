<?php

namespace MyApp\Controllers;

use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\ResourceBenchmark;
use Benchmark\Measure\CPUMeasure;
use Benchmark\Time;

abstract class ControllerTask extends ControllerBase
{
    public function initialize() {}

    public abstract function indexAction();

    public function queriesAction() {
        $type = $this->dispatcher->getParam(0, ['emptytonull'], null) ?: 'sql';

        if (!in_array($type, ['sql', 'orm', 'builder', 'proc'])) {
            return $this->routeBadParam();
        }

        $func = $this->prepareRun($type, $error);
        call_user_func_array(...$func);

        $stat = $this->helper->sessionStatus();

        $queries = [];
        $profiles = $this->helper->showProfiling();
        foreach($profiles as $profile) {
            $queries[] = [
                'id' => intval($profile['query_id']),
                'query' => $profile['query']
            ];
        }
        $queries = array_slice($queries, 0, -1);

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->response->setJsonContent([
            'queries' => $queries,
            'stat' => [
                'byteReceived' => $this->setScaleReceived($stat['byteReceived']),
                'byteSent' => $this->setScaleSend($stat['byteSent']),
                'questions' => [$stat['questions'], '', '']
            ]
        ]);
        return $this->response->send();
    }

    public function baseAction() {
        $repeat = $this->request->getQuery('repeat', ['int', 'trim', 'emptytonull'], null) ?: 1;
        if ($repeat < 1 || $repeat > 1000) {
            $repeat = 1;
        }

        $types = ['sql', 'proc', 'builder', 'orm'];
        $funcs = [];
        $times = [];
        $errors = [];
        foreach ($types as $type) {
            $funcs[$type] = $this->prepareRun($type, $error);
            $errors[$type] = $error;
            $times[$type] = [];
        }

        for($i = $repeat; $i--;) {
            foreach ($types as $type) {
                if ($errors[$type]) continue;

                $func = $funcs[$type];
                $startTime = microtime(true);
                call_user_func_array(...$func);
                $endTime = microtime(true);
                $times[$type][] = round(($endTime - $startTime) * 1e6);
            }
        }

        $avg = [];
        foreach ($times as $type => $time) {
            if ($errors[$type]) {
                $avg[$type] = null;
                continue;
            }

            $sumTime = 0;
            foreach ($time as $t) {
                $sumTime += $t;
            }

            $avg[$type] = round($sumTime / $repeat);
        }

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->response->setJsonContent(
            [
                'avg' => $avg,
                //'times' => $times,
                'param' => $this->request->getQuery(),
                'avgScale' => ['Мікросекунди', 'мкс']
            ]
        );
        return $this->response->send();
    }

    public function benchmarkAction() {
        $type = $this->dispatcher->getParam(0, ['emptytonull'], null) ?: 'sql';

        if (!in_array($type, ['sql', 'orm', 'builder', 'proc'])) {
            return $this->routeBadParam();
        }

        $func = $this->prepareRun($type, $error);

        $cpuMeasure = new CPUMeasure();
        $memoryBaseMeasure = new MemoryBaseMeasure();
        $benchmark = new ResourceBenchmark($memoryBaseMeasure, $cpuMeasure);

        call_user_func_array(...$func);

        $benchmark->end();

        $this->setScaleCpuMeasure($cpuMeasure);
        $this->setScaleMemoryBaseMeasure($memoryBaseMeasure);

        $profiles = $this->helper->showProfiling();
        $pIds = array_map(function ($profile) {
            return $profile['query_id'];
        }, $profiles);
        $profileInfo = $this->helper->showProfileInfo($pIds);
        $statProfiles = [];
        foreach($profiles as $profile) {
            $statProfiles[] = [
                'id' => intval($profile['query_id']),
                'duration' => floatval($profile['duration']),
                'info' => @$profileInfo[$profile['query_id']]
            ];
        }
        unset($profiles);
        unset($pIds);
        unset($profileInfo);

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->response->setJsonContent(
            [
                'error' => $error,
                'exec' => $this->setExec($benchmark->getTimeExec()),
                'cpu' => [
                    'total' => $cpuMeasure->totalPercentage(),
                    'stamp' => $cpuMeasure->parsentList(),
                    'scale' => $cpuMeasure->getScale()
                ],
                'memory' => [
                    'used' => [
                        'max' => $memoryBaseMeasure->getMax(),
                        'stamp' => $memoryBaseMeasure->getList()
                    ],
                    'scale' => $memoryBaseMeasure->getScale()
                ],
                'queries' => $statProfiles
            ]
        );
        return $this->response->send();
    }

    protected function setScaleSend($byte): array {
        return [round($byte / 1024), 'Кілобайти', 'КБ'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MILLI);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MILLI, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MILLI);
    }

    protected abstract function prepareRun($type, &$error): array;

    protected abstract function setTitle();

    protected abstract function getTask();
}