<?php

namespace MyApp\Controllers;


use Benchmark\Measure\CPUMeasure;
use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\Time;

use MyApp\Models\Task4\Orm\Sc4Table1;
use MyApp\Models\Task4\Builder\BuilderModel;
use MyApp\Models\Task4\Proc\ProcModel;
use MyApp\Models\Task4\Sql\SqlModel;

class Task4Controller extends ControllerTask
{
    public function initialize() {}

    public function indexAction() {
        $this->setTitle();
        $tables = ['sc4_table1_sql', 'sc4_table1_proc', 'sc4_table1_builder', 'sc4_table1_orm'];
        $showTable = '';
        $countRecord = [];
        foreach ($tables as $table) {
            $countRecord[$table] = $this->helper->countRecordTable($table);
            $showTable .= $this->helper->showTable($table)['created'] . PHP_EOL;
        }

        $code = [
            'sql' => $this->helper->getFileContent(SqlModel::class),
            'proc' => [
                'created' => $this->helper->showProc('sc4_table1'),
                'code' => $this->helper->getFileContent(ProcModel::class)
            ],
            'builder' => $this->helper->getFileContent(BuilderModel::class),
            'orm' => $this->helper->getFileContent(Sc4Table1::class)
        ];

        $this->view->data = [
            'showTable' => $showTable,
            'countRecord' => $countRecord,
            'code' => $code,
            'metrics' => $this->helper->getAllMetrics(__DIR__ . '/../models/' . $this->getTask())
        ];
    }

    protected function prepareRun($type, &$error): array {
        $error = 0;

        $config = $this->config;
        $pdo = new \PDO(
            "mysql:host={$config['database']['host']};dbname={$config['database']['dbname']};charset={$config['database']['charset']}",
            $config['database']['username'],
            $config['database']['password'],
            [\PDO::ATTR_CASE => \PDO::CASE_LOWER, \PDO::ATTR_PERSISTENT => FALSE, \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_NUM]
        );
        $pdo->query("delete from sc4_table1_$type where id > 10000;");

        $records = $this->request->getQuery('records_count', ['int', 'trim', 'emptytonull'], null);
        $records = intval($records);
        if ($records < 1 || $records > 1000) {
            $records = 1;
        }

        $cb = null;
        if ($type == 'sql') {
            $cb = ["\\MyApp\\Models\\Task4\\Sql\\SqlModel", 'exec'];
        } elseif ($type == 'orm') {
            $cb = ["\\MyApp\\Models\\Task4\\Orm\\Sc4Table1", 'exec'];
        } elseif ($type == 'builder') {
            $cb = ["\\MyApp\\Models\\Task4\\Builder\\BuilderModel", 'exec'];
        } elseif ($type == 'proc') {
            $cb = ["\\MyApp\\Models\\Task4\\Proc\\ProcModel", 'exec'];
        }

        return [$cb, [$records]];
    }

    protected function setScaleSend($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MILLI);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MILLI, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MILLI);
    }

    protected function setTitle() {
        $this->tag->setTitle('Робота з однією таблицею. Вставка даних');
    }

    protected function getTask() {
        return 'Task4';
    }
}