<?php

namespace MyApp\Controllers;


use Benchmark\Measure\CPUMeasure;
use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\Time;

use MyApp\Models\Task3\Orm\Func\Sc3Table1;
use MyApp\Models\Task3\Builder\Func\BuilderModel;
use MyApp\Models\Task3\Proc\Func\ProcModel;
use MyApp\Models\Task3\Sql\Func\SqlModel;

class Task3Controller extends ControllerTask
{
    public function initialize() {}

    public function indexAction() {
        $this->setTitle();
        $tables = ['sc3_table1'];
        $showTable = '';
        $countRecord = [];
        foreach ($tables as $table) {
            $countRecord[$table] = $this->helper->countRecordTable($table);
            $showTable .= $this->helper->showTable($table)['created'] . PHP_EOL;
        }

        $code = [
            'sql' => $this->helper->getFileContent(SqlModel::class),
            'proc' => [
                'created' => $this->helper->showProc('sc3_table1_stored_function'),
                'code' => $this->helper->getFileContent(ProcModel::class)
            ],
            'func' => [
                'created' => $this->helper->showFunc('sc3_table1_func')
            ],
            'udf' => [
                'created' => file_get_contents(__DIR__ . '/../additional/sc3_table1_ufunc.c')
            ],
            'builder' => $this->helper->getFileContent(BuilderModel::class),
            'orm' => $this->helper->getFileContent(Sc3Table1::class)
        ];

        $this->view->data = [
            'showTable' => $showTable,
            'countRecord' => $countRecord,
            'code' => $code,
            'metrics' => $this->helper->getAllMetrics(__DIR__ . '/../models/' . $this->getTask(), '/Func')
        ];
    }

    protected function prepareRun($type, &$error): array {
        $error = 0;

        $cb = null;
        if ($type == 'sql') {
            $typeExec = $this->request->getQuery('sql_type', ['int', 'trim', 'emptytonull'], null);
            if (!in_array($typeExec, [1, 2])) {
                $typeExec = 1;
            }
            $execSpace = $typeExec == 1 ? 'Func' : 'Inline';

            $cb = ["\\MyApp\\Models\\Task3\\Sql\\$execSpace\\SqlModel", 'exec'];
        } elseif ($type == 'orm') {
            $typeExec = $this->request->getQuery('orm_type', ['int', 'trim', 'emptytonull'], null);
            if (!in_array($typeExec, [1, 2])) {
                $typeExec = 1;
            }
            $execSpace = $typeExec == 1 ? 'Func' : 'Inline';

            $cb = ["\\MyApp\\Models\\Task3\\Orm\\$execSpace\\Sc3Table1", 'exec'];
        } elseif ($type == 'builder') {
            $typeExec = $this->request->getQuery('builder_type', ['int', 'trim', 'emptytonull'], null);
            if (!in_array($typeExec, [1, 2])) {
                $typeExec = 1;
            }
            $execSpace = $typeExec == 1 ? 'Func' : 'Inline';

            $cb = ["\\MyApp\\Models\\Task3\\Builder\\$execSpace\\BuilderModel", 'exec'];
        } elseif ($type == 'proc') {
            $typeExec = $this->request->getQuery('proc_type', ['int', 'trim', 'emptytonull'], null);
            if (!in_array($typeExec, [1, 2, 3])) {
                $typeExec = 1;
            }
            $execSpace =  (['Func', 'UFunc', 'Inline'])[$typeExec - 1];

            $cb = ["\\MyApp\\Models\\Task3\\Proc\\$execSpace\\ProcModel", 'exec'];
        }

        return [$cb, []];
    }

    protected function setScaleSend($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MILLI);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MILLI, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MILLI);
    }

    protected function setTitle() {
        $this->tag->setTitle('Робота з однією таблицею. Вибірка всіх даних задовольняючих умову');
    }

    protected function getTask() {
        return 'Task3';
    }
}