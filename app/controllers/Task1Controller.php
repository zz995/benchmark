<?php

namespace MyApp\Controllers;


use Benchmark\Measure\CPUMeasure;
use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\Time;

use MyApp\Models\Task1\Orm\Sc1Table1;
use MyApp\Models\Task1\Builder\BuilderModel;
use MyApp\Models\Task1\Proc\ProcModel;
use MyApp\Models\Task1\Sql\SqlModel;

class Task1Controller extends ControllerTask
{
    public function initialize() {}

    public function indexAction() {
        $this->setTitle();
        $tables = ['sc1_table1'];
        $showTable = '';
        $countRecord = [];
        foreach ($tables as $table) {
            $countRecord[$table] = $this->helper->countRecordTable($table);
            $showTable .= $this->helper->showTable($table)['created'] . PHP_EOL;
        }

        $code = [
            'sql' => $this->helper->getFileContent(SqlModel::class),
            'proc' => [
                'created' => $this->helper->showProc('sc1_table1'),
                'code' => $this->helper->getFileContent(ProcModel::class)
            ],
            'builder' => $this->helper->getFileContent(BuilderModel::class),
            'orm' => $this->helper->getFileContent(Sc1Table1::class)
        ];

        $this->view->data = [
            'showTable' => $showTable,
            'countRecord' => $countRecord,
            'code' => $code,
            'metrics' => $this->helper->getAllMetrics(__DIR__ . '/../models/' . $this->getTask())
        ];
    }

    protected function prepareRun($type, &$error): array {
        $error = 0;

        $cb = null;
        if ($type == 'sql') {
            $cb = ["\\MyApp\\Models\\Task1\\Sql\\SqlModel", 'exec'];
        } elseif ($type == 'orm') {
            $cb = ["\\MyApp\\Models\\Task1\\Orm\\Sc1Table1", 'exec'];
        } elseif ($type == 'builder') {
            $cb = ["\\MyApp\\Models\\Task1\\Builder\\BuilderModel", 'exec'];
        } elseif ($type == 'proc') {
            $cb = ["\\MyApp\\Models\\Task1\\Proc\\ProcModel", 'exec'];
        }

        return [$cb, [10, 30, 50, 'field2_desc']];
    }

    protected function setScaleSend($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MILLI);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MILLI, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MILLI);
    }

    protected function setTitle() {
        $this->tag->setTitle('Робота з однією таблицею. Вибірка всіх даних задовольняючих умову, та сортування за певним полем');
    }

    protected function getTask() {
        return 'Task1';
    }
}