<?php

namespace MyApp\Controllers;

use Benchmark\Measure\CPUMeasure;
use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\Time;
use MyApp\Templates\Task6\PrepareModel;

use MyApp\Models\Task6\Table1\Orm\Table1;
use MyApp\Models\Task6\Table1\Orm\Table2;
use MyApp\Models\Task6\Table1\Builder\BuilderModel;
use MyApp\Models\Task6\Table1\Proc\ProcModel;
use MyApp\Models\Task6\Table1\Sql\SqlModel;

class Task6Controller extends ControllerTask
{
    public function initialize() {}

    public function indexAction() {
        //$prepareModal = new PrepareModel(15);

        $this->setTitle();
        $tables = ['sc6_table1', 'sc6_table2'];
        $showTable = '';
        $countRecord = [];
        foreach ($tables as $table) {
            $countRecord[$table] = $this->helper->countRecordTable($table);
            $showTable .= $this->helper->showTable($table)['created'] . PHP_EOL . PHP_EOL;
        }

        $code = [
            'sql' => $this->helper->getFileContent(SqlModel::class),
            'proc' => [
                'created' => $this->helper->showProc('sc6_table1_t1'),
                'code' => $this->helper->getFileContent(ProcModel::class)
            ],
            'builder' => $this->helper->getFileContent(BuilderModel::class),
            'orm' => $this->helper->getFileContent(Table1::class, Table2::class)
        ];

        $this->view->data = [
            'showTable' => $showTable,
            'countRecord' => $countRecord,
            'code' => $code,
            'metrics' => $this->helper->getAllMetrics(__DIR__ . '/../models/' . $this->getTask() . '/Table1')
        ];
    }

    public function commonAction() {
        $this->tag->setTitle('Вибір записів з головної таблиці та приєднання залежних таблиць, загальні дані');

        $this->view->params = [
            ['name' => 'records_main', 'title' => 'Кількість записів (головна таблиця)', 'values' => [1, 3, 5, 10, 15, 20, 25, 50, 75, 100, 125, 150, 175, 200]],
            ['name' => 'records_second', 'title' => 'Кількість записів (залежна таблиця)', 'values' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]],
            ['name' => 'tables_count', 'title' => 'Кількість залежних таблиць', 'values' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]]
        ];
    }

    protected function prepareRun($type, &$error): array {
        $recordsMain = $this->request->getQuery('records_main', ['int', 'trim', 'emptytonull'], null);
        $recordsMainAllow = [1, 3, 5, 10, 15, 20, 25, 50, 75, 100, 125, 150, 175, 200];
        if (!in_array($recordsMain, $recordsMainAllow)) {
            $recordsMain = @$recordsMainAllow[0];
        }

        $tablesCount = $this->request->getQuery('tables_count', ['int', 'trim', 'emptytonull'], null);
        $tablesCount = intval($tablesCount);
        if ($tablesCount < 1 || $tablesCount > 15) {
            $tablesCount = 1;
        }

        $recordsSecond = $this->request->getQuery('records_second', ['int', 'trim', 'emptytonull'], null);
        $recordsSecondAllow = [];
        $recordsSecondIds = [];
        for($i = 0; $i <= 15; $i++) {
            $recordsSecondAllow[] = $i;
            $recordsSecondIds[] = 1 + 200 * $i;
        }
        $startId = @array_combine($recordsSecondAllow, $recordsSecondIds)[$recordsSecond] ?: $recordsSecondIds[0];
        $endId = $startId + $recordsMain - 1;

        $error = 0;
        if ($type == 'proc') {
            $constr =  pow($recordsSecond, $tablesCount);
            $limit = 3e4;
            if ($constr > $limit || ($recordsMain * $constr) > $limit) {
                $error = 1;
                return [function() { return null; }, []];
            }
        }
        if ($type == 'orm') {
            if ($tablesCount * $recordsMain > 500) {
                $error = 1;
                return [function() { return null; }, []];
            }
        }

        $params = [$startId, $endId];
        $cb = null;
        if ($type == 'sql') {
            $cb = ["\\MyApp\\Models\\Task6\\Table{$tablesCount}\\Sql\\SqlModel", 'exec'];
        } elseif ($type == 'orm') {
            $cb = ["\\MyApp\\Models\\Task6\\Table{$tablesCount}\\Orm\\Table1", 'exec'];
        } elseif ($type == 'builder') {
            $cb = ["\\MyApp\\Models\\Task6\\Table{$tablesCount}\\Builder\\BuilderModel", 'exec'];
        } elseif ($type == 'proc') {
            $cb = ["\\MyApp\\Models\\Task6\\Table{$tablesCount}\\Proc\\ProcModel", 'exec'];
        }

        return [$cb, $params];
    }

    protected function setScaleSend($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MICRO);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MICRO, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MICRO);
    }

    protected function setTitle() {
        $this->tag->setTitle('Робота з декількома таблицями. Вибір записів з головної таблиці та приєднання залежних таблиць');
    }

    protected function getTask() {
        return 'Task6';
    }
}