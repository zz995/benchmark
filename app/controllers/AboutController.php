<?php

namespace MyApp\Controllers;


class AboutController extends ControllerBase
{
    public function initialize() {
    }

    public function indexAction() {
        $this->tag->setTitle('Про дослідження');

        $this->view->navbar = 'about';
    }
}