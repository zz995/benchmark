<?php

namespace MyApp\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    protected function initialize() {
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->view->navbar = '';
    }

    public function getMethod() {
        $method = 'GET';
        if ($this->request->isPost() && $this->request->get('_method', ['trim', 'emptytonull'], null) == 'DELETE') {
            $method = 'DELETE';
        } elseif ($this->request->isPost()) {
            $method = 'POST';
        }

        return $method;
    }

    public function routeAccessDined() {
        return $this->dispatcher->forward(array(
            'namespace' => 'MyApp\Controllers',
            'controller' => 'error',
            'action' => 'accessDenied',
            'params' => array('message' => 'Access denied')
        ));
    }

    public function routeNotFound() {
        return $this->dispatcher->forward(array(
            'namespace' => 'MyApp\Controllers',
            'controller' => 'error',
            'action' => 'notFound',
            'params' => array('message' => 'Not found')
        ));
    }

    public function routeBadParam() {
        return $this->dispatcher->forward(array(
            'namespace' => 'MyApp\Controllers',
            'controller' => 'error',
            'action' => 'badParam',
            'params' => array('message' => 'Bad param')
        ));
    }
}