<?php

namespace MyApp\Controllers;

class InfoController extends ControllerBase
{
    public function initialize() {
    }

    public function indexAction() {
        $this->tag->setTitle('Системна інформація');

        $this->view->navbar = 'info';
        $this->view->cpuInfo = $this->helper->cpuInfo();
        $this->view->memInfo = $this->helper->memInfo();
        $this->view->mysqlInfo = $this->helper->mysqlInfo();
    }

    public function prepareAction() {
        if (!$this->request->isPost()) {
            return $this->routeNotFound();
        }

        $this->helper->prepareDb();

        $ref = $this->request->getHTTPReferer();
        return $this->response->redirect($ref);
    }
}