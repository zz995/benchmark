<?php

namespace MyApp\Controllers;

use MyApp\Forms\ContactForm;
use MyApp\Library\ContactNetworks;
use MyApp\Models\Contact;
use MyApp\Models\Network;

class IndexController extends ControllerBase
{
    public function initialize() {
    }

    public function indexAction() {
        $this->tag->setTitle('Способи взаємодії з базою даних');
        $this->view->navbar = 'list';
    }
}