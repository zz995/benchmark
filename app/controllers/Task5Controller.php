<?php

namespace MyApp\Controllers;

use Benchmark\Measure\CPUMeasure;
use Benchmark\Measure\MeasureInterface;
use Benchmark\Measure\MemoryBaseMeasure;
use Benchmark\Time;
use Benchmark\TimeBenchmark;
use MyApp\Models\Task5\Table1\Orm\Table1;
use MyApp\Models\Task5\Table1\Orm\Table2;
use MyApp\Models\Task5\Table1\Builder\BuilderModel;
use MyApp\Models\Task5\Table1\Proc\ProcModel;
use MyApp\Models\Task5\Table1\Sql\SqlModel;
use MyApp\Templates\Task5\PrepareModel;
use Regression\Matrix;
use Regression\Regression;

class Task5Controller extends ControllerTask
{
    public function initialize() {}

    public function indexAction() {
        //$prepareModal = new PrepareModel(15);

        $this->setTitle();
        $tables = ['sc5_table1', 'sc5_table2'];
        $showTable = '';
        $countRecord = [];
        foreach ($tables as $table) {
            $countRecord[$table] = $this->helper->countRecordTable($table);
            $showTable .= $this->helper->showTable($table)['created'] . PHP_EOL . PHP_EOL;
        }

        $code = [
            'sql' => $this->helper->getFileContent(SqlModel::class),
            'proc' => [
                'created' => $this->helper->showProc('sc5_table1_t1'),
                'code' => $this->helper->getFileContent(ProcModel::class)
            ],
            'builder' => $this->helper->getFileContent(BuilderModel::class),
            'orm' => $this->helper->getFileContent(Table1::class, Table2::class)
        ];

        $this->view->data = [
            'showTable' => $showTable,
            'countRecord' => $countRecord,
            'code' => $code,
            'metrics' => $this->helper->getAllMetrics(__DIR__ . '/../models/' . $this->getTask() . '/Table1')
        ];
    }

    public function commonAction() {
        $this->tag->setTitle('Вибір одного запису з головної таблиці по ідентифікатору та приєднання залежних таблиць, загальні дані');

        $this->view->params = [
            ['name' => 'countTable', 'title' => 'Приєднано таблиць', 'values' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]],
            ['name' => 'records', 'title' => 'Кількість записів', 'values' => [1, 5, 10, 50, 100, 500]]
        ];
    }

    protected function prepareRun($type, &$error): array {
        $error = 0;

        $count = $this->request->getQuery('count', ['int', 'trim', 'emptytonull'], null);
        $count = intval($count);
        if ($count < 1 || $count > 16) {
            $count = 1;
        }

        $records = $this->request->getQuery('records', ['int', 'trim', 'emptytonull'], null);
        $recordAllowValues = [1, 5, 10, 50, 100, 500];
        $recordIds = [1, 2, 3, 4, 5, 6];
        $recordId = @array_combine($recordAllowValues, $recordIds)[$records] ?: $recordIds[0];

        $params = [$recordId];
        $cb = null;
        if ($type == 'sql') {
            $cb = ["\\MyApp\\Models\\Task5\\Table{$count}\\Sql\\SqlModel", 'exec'];
        } elseif ($type == 'orm') {
            $cb = ["\\MyApp\\Models\\Task5\\Table{$count}\\Orm\\Table1", 'exec'];
        } elseif ($type == 'builder') {
            $cb = ["\\MyApp\\Models\\Task5\\Table{$count}\\Builder\\BuilderModel", 'exec'];
        } elseif ($type == 'proc') {
            $cb = ["\\MyApp\\Models\\Task5\\Table{$count}\\Proc\\ProcModel", 'exec'];
        }

        return [$cb, $params];
    }

    protected function setScaleSend($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setScaleReceived($byte): array {
        return [$byte, 'Байти', 'Б'];
    }

    protected function setExec(Time $time): array {
        return $time->getTimeScale(Time::TIME_MICRO);
    }

    protected function setScaleMemoryBaseMeasure(MeasureInterface $memoryBaseMeasure) {
        $memoryBaseMeasure->setScale(MemoryBaseMeasure::TIME_MICRO, MemoryBaseMeasure::MEMORY_KBYTE);
    }

    protected function setScaleCpuMeasure(MeasureInterface $cpuMeasure) {
        $cpuMeasure->setScale(CPUMeasure::TIME_MICRO);
    }

    protected function setTitle() {
        $this->tag->setTitle('Робота з декількома таблицями. Вибір одного запису з головної таблиці по ідентифікатору та приєднання залежних таблиць');
    }

    protected function getTask() {
        return 'Task5';
    }
}