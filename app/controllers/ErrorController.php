<?php

namespace MyApp\Controllers;

class ErrorController extends ControllerBase
{
    public function initialize() {

    }

    public function indexAction() {

        $err = $this->dispatcher->getParam('err');
        $this->logger->error($err->getMessage() . ' in ' . $err->getFile() . ' on line ' . $err->getLine());

        $this->response->setStatusCode(500);

        if ($this->request->isAjax()){
            $this->response->setJsonContent(['error' => 'Ошибка сервера']);
            return $this->response->send();
        }

        $this->response->send();
    }

    public function notFoundAction()
    {
        $this->response->setStatusCode(404);

        if ($this->request->isAjax()){
            $this->response->setJsonContent(['error' => 'Не найдено']);
            return $this->response->send();
        }

        $this->response->send();
    }

    public function accessDeniedAction() {
        $this->response->setStatusCode(403);

        if ($this->request->isAjax()){
            $this->response->setJsonContent(['error' => 'Нет доступа']);
            return $this->response->send();
        }

        $this->response->send();
    }

    public function badParamAction() {
        $this->response->setStatusCode(400);

        if ($this->request->isAjax()){
            $this->response->setJsonContent(['error' => 'Плохой запрос']);
            return $this->response->send();
        }

        $this->response->send();
    }
}