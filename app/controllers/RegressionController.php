<?php

namespace MyApp\Controllers;

use Regression\Matrix;
use Regression\Regression;

class RegressionController extends ControllerBase
{
    public function initialize() {

    }

    public function multivariateAction() {
        $data = $this->request->getJsonRawBody(true);

        if (!is_array(@$data['x']) || !is_array(@$data['y'])) {
            return $this->routeBadParam();
        }

        $x = $data['x'];
        $y = $data['y'];

        $regression = new Regression();
        $regression->setX(new Matrix($x));
        $regression->setY(new Matrix($y));

        @$regression->exec();

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        $this->response->setJsonContent([
            'coefficients' => $regression->getCoefficients()->getData(),
            'rquare' => $regression->getRSQUARE(),
            'f' => $regression->getF(),
            'stderrors' => $regression->getStandardError(),
            'sse' => $regression->getSSE(),
            'ssr' => $regression->getSSR(),
            'ssto' => $regression->getSSTO(),
            'tstats' => $regression->getTStats(),
            'pvalues' => $regression->getPValues(),
        ]);
        return $this->response->send();
    }
}