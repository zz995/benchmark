<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    array(
        'MyApp\Controllers' => __DIR__ . '/../controllers/',
        'MyApp\Models' => __DIR__ . '/../models/',
        'MyApp\Templates' => __DIR__ . '/../templates/',
        'MyApp\Plugins' => __DIR__ . '/../plugins/',
        'MyApp\Tag' => __DIR__ . '/../tag/',
        'MyApp\Library' => __DIR__ . '/../library/',
        'Regression' => __DIR__ . '/../library/regression/',
    )
)->register();