<?php

use Phalcon\Logger;

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'   => 'Mysql',
        'port'      => 3306,
        'host'      => '127.0.0.1',
        'username'  => 'root',
        'password'  => 'zz',
        'dbname'    => 'zz',
        'charset'   => 'utf8'
    ),
    'application' => array(
        'env' => 'debug',
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'formsDir'       => __DIR__ . '/../../app/forms/',
        'baseUri'        => '/',
    ),
    'logger' => [
        'path'     => __DIR__ . '/../logs/',
        'format'   => '%date% [%type%] %message%',
        'date'     => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'error.log',
    ],
));