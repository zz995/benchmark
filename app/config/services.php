<?php


use MyApp\Library\Helper;
use Phalcon\Di\FactoryDefault;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Events\Manager as Manager;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as FormatterLine;
use Phalcon\Flash\Session as FlashSession;

use MyApp\Plugins\NotFoundPlugin;
use MyApp\Tag\FunctionView as Tag;

$di = new FactoryDefault();


$di->set('router', function () {
    return require __DIR__ . '/routes/index.php';
}, true);

$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
}, true);

$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiedSeparator' => '_',
                "lifetime" => 0,
                'compileAlways'     => false,
            ));

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

$di->set('tag', function() {
    return new Tag();
});

$di->setShared('db', function () use ($config) {
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $connection = new $class([
        'host'     => $config->database->host,
        'port'     => $config->database->port,
        'charset'  => $config->database->charset,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'options' => [PDO::ATTR_CASE => PDO::CASE_LOWER, PDO::ATTR_PERSISTENT => FALSE, PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC]
    ]);

    return $connection;
});

$di->set('helper', function () use ($config) {
    return new Helper();
});

$di->set('modelsMetadata', function () use ($config) {
    return new MetaDataAdapter();
});

$di->set('flash', function () {
    $flash = new Flash([
        'error' => 'uk-alert uk-alert-danger uk-container uk-container-center uk-margin-top padding-top padding-bottom',
        'success' => 'uk-alert uk-alert-success uk-container uk-container-center uk-margin-top padding-top padding-bottom',
        'notice' => '',
        'warning' => 'uk-alert uk-alert-warning uk-container uk-container-center uk-margin-top padding-top padding-bottom'
    ]);
    $flash->setAutoescape(false);

    return $flash;
});

$di->set('flashSession', function () {
    $flash = new FlashSession([
        'error' => 'uk-alert uk-alert-danger uk-container uk-container-center uk-margin-top padding-top padding-bottom',
        'success' => 'uk-alert uk-alert-success uk-container uk-container-center uk-margin-top padding-top padding-bottom',
        'notice' => '',
        'warning' => 'uk-alert uk-alert-warning uk-container uk-container-center uk-margin-top padding-top padding-bottom'
    ]);
    $flash->setAutoescape(false);

    return $flash;
});

$di->set('session', function(){
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

$di->setShared('config', function () use ($config) {
    return $config;
});

$di->setShared('dispatcher', function () {

    $eventsManager = new Manager();

    $eventsManager->attach(
        "dispatch:beforeException",
        new NotFoundPlugin()
    );

    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('MyApp\Controllers');
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

$di->set('logger', function ($filename = null, $format = null)  use ($config) {
    $format = $format ?: $config->get('logger')->format;
    $filename = trim($filename ?: $config->get('logger')->filename, '\\/');
    $path = rtrim($config->get('logger')->path, '\\/') . DIRECTORY_SEPARATOR;
    $formatter = new FormatterLine($format, $config->get('logger')->date);
    $logger = new FileLogger($path . $filename);
    $logger->setFormatter($formatter);
    $logger->setLogLevel($config->get('logger')->logLevel);
    return $logger;
});

$di->setShared('filter', function() {

    $filter = new \Phalcon\Filter();

    $filter->add('emptytonull', function ($value) {
        if($value === '') {
            return null;
        } else {
            return $value;
        }
    });

    $filter->add('stringtoint', function ($value) {
        return intval($value);
    });

    $filter->add('stringtofloat', function ($value) {
        return floatval($value);
    });

    $filter->add('stringtobool', function ($value) {
        return intval($value) == 1 ? 1 : 0;
    });

    $filter->add('page', function ($page) {
        $page = intval($page);
        if ($page < 1) {
            $page = 1;
        }
        return $page;
    });

    $filter->add('limit', function ($limit) {
        $limit = intval($limit);
        if ($limit < 1 || $limit > 100) {
            $limit = 20;
        }
        return $limit;
    });

    return $filter;
});