<?php

namespace Benchmark;

class Process {
    public $pipes = null;
    public $resource = null;
    public $exitCode = null;
    public $env = null;
    public $cmd = '';
    private $descriptors = array(
        0 => array ("pipe", "r"),
        1 => array ("pipe", "w"),
        2 => array ("pipe", "w")
    );

    public function __construct(string $cmd, array $env = null) {
        $this->cmd = $cmd;
        $this->env = $env;
        $this->resource = proc_open($this->cmd, $this->descriptors, $this->pipes, null, $this->env);
    }

    public function writeContent($data) {
        if (is_array($data)) {
            $data = serialize($data);
        }
        fwrite($this->pipes[0], $data);
    }

    public function getPartContent(int $size = 4096) {
        $data = fread($this->pipes[1], 4096);
        return $data;
    }

    public function readAllContent() {
        $content = '';
        $s = '';
        while(!feof($this->pipes[1])) {
            $s = fread($this->pipes[1], 16384);
            $content .= $s;
        }

        return $content;
    }

    public function getContent() {
        $data = stream_get_contents($this->pipes[1]);
        return $data;
    }

    public function isRunning() {
        $status = proc_get_status($this->resource);
        if ($status['running'] === FALSE && $this->exitCode === NULL)
            $this->exitCode = $status['exitcode'];

        return $status['running'];
    }

    public function nonBlocking() {
        stream_set_blocking($this->pipes[1], 0);
        stream_set_blocking($this->pipes[2], 0);
    }

    public function closeStream() {
        fclose($this->pipes[0]);
        fclose($this->pipes[1]);
        fclose($this->pipes[2]);
    }

    public function close() {
        $this->closeStream();
        if (proc_close($this->resource) == -1 && $this->isRunning()) {
            $this->kill();
        }
    }

    public function kill() {
        $pstatus = proc_get_status($this->resource);
        $pid = $pstatus['pid'];
        return exec("kill -9 $pid");
    }

    public function terminate() {
        proc_terminate($this->resource);
    }
}