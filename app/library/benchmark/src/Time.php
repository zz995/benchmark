<?php

namespace Benchmark;

class Time {
    const TIME_MILLI = 1;
    const TIME_MICRO = 2;
    protected $microTimeStamp;

    public function __construct(float $default = null) {
        if (is_null($default)) {
            $default = microtime(true);
        }
        $this->microTimeStamp = $default;
    }

    public function getMillisecond(): int {
        return round($this->microTimeStamp * 1e3);
    }

    public function getMicrosecond(): int {
        return round($this->microTimeStamp * 1e6);
    }

    public function get() {
        return $this->microTimeStamp;
    }

    public function diff(Time $time): Time {
        return new Time($this->get() - $time->get());
    }

    public function getTimeScale(int $timeScale) {
        $map = [
            self::TIME_MILLI => ['Мілісекунди', 'мс'],
            self::TIME_MICRO => ['Мікросекунди', 'мкс']
        ];

        return [
            'time' => $timeScale == self::TIME_MILLI ? $this->getMillisecond() : $this->getMicrosecond(),
            'scale' => $map[$timeScale]
        ];
    }
}