<?php

class FixingTool implements FixingToolInterface {

    const MAX = 100;

    private $data = [];
    private $space = 1;
    private $skip = 0;

    public function __construct() {

    }

    public function fix(array $val) {
        $this->skip++;
        if (($this->skip % $this->space) !== 0) {
            return;
        }
        $this->skip = 0;

        $this->data[] = $val;

        if (count($this->data) == self::MAX) {
            $this->space *= 2;
            $this->skip = self::MAX % $this->space;
            $data = [];
            foreach ($this->data as $k => $d) {
                if (!($k & 1)) {
                    $data[] = $d;
                }
            }

            //$data = array_filter($this->data, function ($val) {return !($val & 1);}, ARRAY_FILTER_USE_KEY);
            $this->data = $data;
        }
    }

    public function getData(): array {
        return $this->data;
    }
}