<?php

namespace Benchmark;

use Benchmark\Measure\MeasureInterface;

class ResourceBenchmark extends TimeBenchmark {

    const SLEEP_PROCESS_MSEC = 10;
    protected $process;
    protected $measures;

    public function __construct(MeasureInterface ...$measures) {
        $this->measures = $measures;
        $this->process = new Process('php ' . __DIR__ . '/FixResource.php', array(
            'BENCHMARK_SLEEP' => self::SLEEP_PROCESS_MSEC
        ));
        $this->process->writeContent(serialize(['measures' => $measures]) . PHP_EOL);
        if($this->process->getPartContent() != 1) {
            $this->process->close();
            throw new \RuntimeException('Process not started');
        };
        parent::__construct();
    }

    public function end() {
        parent::end();

        if (!$this->process->isRunning()) {
            throw new \LogicException('Process not started');
        }
        $this->process->writeContent(['type' => 'stop', 'data' => [null]]);

        $data = unserialize($this->process->getContent());
        foreach ($data as $item) {
            $time = array_shift($item);
            for($i = 0, $l = count($this->measures); $i < $l; $i++) {
                $this->measures[$i]->add($time, $item[$i]);
            }
            unset($i);
            unset($l);
        }

        $this->process->close();
    }
}