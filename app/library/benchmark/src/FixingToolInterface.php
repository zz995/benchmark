<?php

interface FixingToolInterface {
    public function fix(array $val);
    public function getData(): array;
}