<?php

require_once (__DIR__ . '/FixingToolInterface.php');
require_once (__DIR__ . '/FixingTool.php');
require_once (__DIR__ . '/Measure/MeasureInterface.php');
require_once (__DIR__ . '/Measure/MeasureTrait.php');
require_once (__DIR__ . '/Measure/CPUMeasure.php');
require_once (__DIR__ . '/Measure/MemoryBaseMeasure.php');

class FixResource {
    protected $settings = [];
    protected $fixingTool;
    protected $handlerStarted = 0;
    protected $handlerWorking = 0;
    protected $stdin = null;

    static private $instance = null;

    protected function __construct() {
        $this->fixingTool = new FixingTool();
        $this->settings = [
            'sleep' => getenv('BENCHMARK_SLEEP') ?: 10000
        ];
        $this->stdin = fopen('php://stdin','rb');
        $serMeasures = fgets($this->stdin);
        $this->measures = empty($serMeasures) ? [] : unserialize($serMeasures)['measures'];

        $this->handler();
    }

    protected function handler() {
        while(true) {
            $read   = array($this->stdin);
            $write  = NULL;
            $except = NULL;

            $command = false;
            if (false === ($num_changed_streams = stream_select($read, $write, $except, 0,  $this->settings['sleep']))) {
                $this->error();
            } elseif ($num_changed_streams > 0) {
                $command = $this->getCommand();
            }

            if (isset($command) && $command['type'] == 'stop') {
                $this->runMeasures();
                $this->stop();
            }

            if (!$this->handlerStarted) {
                $this->handlerStarted();
            }

            $this->runMeasures();
        }
    }

    protected function runMeasures() {
        $data = [microtime(true)];

        foreach ($this->measures as $measure) {
            $data[] = $measure->get();
        }

        $this->fixingTool->fix($data);
    }

    protected function getCommand() {
        $command = unserialize(fread($this->stdin, 4096));
        fclose($this->stdin);
        return $command;
    }

    protected function error() {
        /* Обработка ошибок */
    }

    protected function handlerStarted() {
        $this->handlerStarted = 1;
        $fo = fopen('php://stdout','w');
        fwrite($fo, 1);
        fclose($fo);
    }

    protected function stop() {
        echo serialize($this->fixingTool->getData());
        exit();
    }

    static public function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }
}

FixResource::getInstance();