<?php

namespace Benchmark\Measure;

use Benchmark\Time;

class MemoryBaseMeasure implements MeasureInterface {
    use MeasureTrait;

    const MEMORY_BYTE = 1;
    const MEMORY_KBYTE = 2;
    const MEMORY_MBYTE = 3;

    protected $timeType = self::TIME_MILLI;
    protected $memoryType = self::MEMORY_KBYTE;
    protected $measures = [];

    public function get() {
        $stats = @file_get_contents("/proc/meminfo");
        if ($stats === false) {
            throw new \RuntimeException('Can\'t read meminfo');
        }

        $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
        $stats = explode("\n", $stats);

        $memoryTotal = $memoryFree = 0;
        foreach ($stats as $statLine) {
            $statLineData = explode(":", trim($statLine));
            if (count($statLineData) == 2 && trim($statLineData[0]) == "MemTotal") {
                $memoryTotal = trim($statLineData[1]);
                $memoryTotal = explode(" ", $memoryTotal);
                $memoryTotal = $memoryTotal[0];
                $memoryTotal *= 1024;
            }

            if (count($statLineData) == 2 && trim($statLineData[0]) == "MemFree") {
                $memoryFree = trim($statLineData[1]);
                $memoryFree = explode(" ", $memoryFree);
                $memoryFree = $memoryFree[0];
                $memoryFree *= 1024;
            }
        }

        return $memoryTotal - $memoryFree;
    }

    public function getList() {
        $data = [];
        if (empty($this->measures)) {
            return $data;
        }

        foreach ($this->measures as $measure) {
            list($time, $memory) = $this->normalizeData(...$measure);
            $data[] = $this->prepareData($time, $memory);
        }

        return $data;
    }

    public function getMax() {
        if (count($this->measures) == 0) {
            return null;
        }

        $max = $this->measures[0];
        foreach ($this->measures as $measure) {
            if ($max[1] < $measure[1]) {
                $max = $measure;
            }
        }

        list($time, $memory) = $this->normalizeData(...$max);
        return $this->prepareData($time, $memory);
    }

    public function getAvg() {
        
    }

    public function add($timestamp, $measure) {
        $this->measures[] = [new Time($timestamp), $measure];
    }

    public function setScale($time = null, $memory = null) {
        if (isset($time)) {
            $this->timeType = $time;
        } else {
            $this->setScaleTimeAuto();
        }

        if (isset($memory)) {
            $this->memoryType = $memory;
        } else {
            $this->setScaleMemoryAuto();
        }
    }

    public function getScale(): array {
        return [$this->getScaleTime(), $this->getScaleMemory()];
    }

    protected function getScaleMemory() {
        $map = [
            self::MEMORY_BYTE => ['Байти', 'Б'],
            self::MEMORY_KBYTE => ['Кілобайти', 'КБ'],
            self::MEMORY_MBYTE => ['Мегабайти', 'МБ']
        ];

        return @$map[$this->memoryType] ?: ['Байти', 'Б'];
    }

    protected function setScaleMemoryAuto() {
        if (empty($this->measures)) return;

        $min = $this->measures[0][1];
        $max = $this->measures[0][1];
        foreach ($this->measures as $measure) {
            if ($max < $measure[1]) {
                $max = $measure[1];
            }
        }

        $diff = $max - $min;
        if ($diff > (1024 * 1024 * 100)) {
            $this->memoryType = self::MEMORY_MBYTE;
        } elseif ($diff > (1024 * 100)) {
            $this->memoryType = self::MEMORY_KBYTE;
        } else {
            $this->timeType = self::MEMORY_BYTE;
        }
    }

    protected function normalizeData(Time $time, $memory) {
        if (empty($this->measures)) {
            throw new \LogicException('Measures is empties');
        }

        list($startTime, $startMemory) = $this->measures[0];

        $curTime = $time->diff($startTime);
        $curMem = $memory - $startMemory;
        if ($curMem < 0) {
            $curMem = 0;
        }

        return [$curTime, $curMem];
    }
    protected function prepareData(Time $curTime, $curMem) {
        $item = [];

        if ($this->timeType == self::TIME_MILLI) {
            $item[] = $curTime->getMillisecond();
        } elseif ($this->timeType == self::TIME_MICRO) {
            $item[] = $curTime->getMicrosecond();
        } else {
            $item[] = $curTime->get();
        }

        if ($this->memoryType == self::MEMORY_BYTE) {
            $item[] = $curMem;
        } elseif ($this->memoryType == self::MEMORY_KBYTE) {
            $item[] = round($curMem / 1024);
        } elseif ($this->memoryType == self::MEMORY_MBYTE) {
            $item[] = round($curMem / 1024 / 1024);
        } else {
            $item[] = $curMem;
        }

        return $item;
    }
}