<?php

namespace Benchmark\Measure;

trait MeasureTrait {
    protected function setScaleTimeAuto() {
        if (empty($this->measures)) return;

        $first = $this->measures[0][0];
        $last = $this->measures[count($this->measures) - 1][0];

        $diff = $last->diff($first)->getMicrosecond();
        if ($diff > 1e5) {
            $this->timeType = self::TIME_MILLI;
        } else {
            $this->timeType = self::TIME_MICRO;
        }
    }

    protected function getScaleTime() {
        $map = [
            self::TIME_MILLI => ['мс', 'Мілісекунди'],
            self::TIME_MICRO => ['мкс', 'Мікросекунди']
        ];

        return @$map[$this->timeType] ?: ['мс', 'Мілісекунди'];
    }
}