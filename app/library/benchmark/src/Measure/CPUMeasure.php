<?php

namespace Benchmark\Measure;

use Benchmark\Time;

class CPUMeasure implements MeasureInterface {
    use MeasureTrait;

    const CPU_PARSENT = 1;

    protected $timeType = self::TIME_MILLI;
    protected $cpuType = self::CPU_PARSENT;
    protected $measures = [];

    public function get() {
        $buf = @file_get_contents('/proc/stat');
        $nums = preg_split(
            "/\\s+/",
            substr(
                $buf,
                0,
                strpos($buf, "\n")
            )
        );
        return [$nums[1] + $nums[2] + $nums[3], intval($nums[4])];
    }

    public function add($timestamp, $measure) {
        $this->measures[] = [new Time($timestamp), $measure];
    }

    public function parsentList() {
        $data = [];

        $startTime = 0;
        if (count($this->measures) > 0) {
            $startTime = $this->measures[0][0];
        }

        for($i = 1; $i < count($this->measures); $i++) {
            $cur = $this->measures[$i][1];
            $prev = $this->measures[$i - 1][1];

            $diffTime = $this->measures[$i][0]->diff($startTime);
            $loadParsent = $this->calcPercentage($cur, $prev);
            if (is_null($loadParsent)) continue;

            $data[] = $this->prepareData($diffTime, $loadParsent);
        }

        return $data;
    }

    protected function calcPercentage($cur, $prev) {
        $diffTotal = $cur[0] + $cur[1] - ($prev[0] + $prev[1]);
        $diffIdle = $cur[1] - $prev[1];
        if (($diffTotal - $diffIdle) == 0) return null;

        return 100 * ($diffTotal - $diffIdle) / $diffTotal;
    }

    public function totalPercentage() {
        if (empty($this->measures)) {
            return null;
        }

        $first = $this->measures[0];
        $last = $this->measures[count($this->measures) - 1];

        $diffTime = $last[0]->diff($first[0]);
        $loadParsent = $this->calcPercentage($first[1], $last[1]);

        return $this->prepareData($diffTime, $loadParsent);
    }

    protected function prepareData(Time $curTime, $parsent) {
        $item = [];

        if ($this->timeType == self::TIME_MILLI) {
            $item[] = $curTime->getMillisecond();
        } elseif ($this->timeType == self::TIME_MICRO) {
            $item[] = $curTime->getMicrosecond();
        } else {
            $item[] = $curTime->get();
        }

        $item[] = round($parsent);

        return $item;
    }

    public function setScale($time = null, $cpu = null) {
        if (isset($time)) {
            $this->timeType = $time;
        } else {
            $this->setScaleTimeAuto();
        }

        $this->cpuType = $cpu;
    }

    public function getScale(): array {
        return [$this->getScaleTime(), ['Проценти', '%']];
    }
}