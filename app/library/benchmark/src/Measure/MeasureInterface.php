<?php

namespace Benchmark\Measure;

interface MeasureInterface {
    const TIME_MILLI = 1;
    const TIME_MICRO = 2;

    public function get();
    public function add($timestamp, $measure);
    public function setScale($x = null, $y = null);
    public function getScale(): array;
}