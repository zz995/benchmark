<?php

namespace Benchmark;

class TimeBenchmark {

    /** @var Time $startTimeStamp */
    protected $startTimeStamp;
    /** @var Time $startTimeStamp */
    protected $endTimeStamp;

    public function __construct() {
        $this->setStartTimeStamp();
    }

    public function end() {
        $this->setEndTimeStamp();
    }

    public function getTimeExec(): Time {
        if (is_null($this->endTimeStamp)) {
            throw  new \LogicException('Benchmark don\'t stop');
        }
        return $this->endTimeStamp->diff($this->startTimeStamp);
    }

    private function setStartTimeStamp() {
        $this->startTimeStamp = new Time();
        $this->endTimeStamp = null;
    }

    private function setEndTimeStamp() {
        $this->endTimeStamp = new Time();
    }
}