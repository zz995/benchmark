<?php

require_once(dirname(__FILE__) . "/src/Process.php");
require_once(dirname(__FILE__) . "/src/Time.php");
require_once(dirname(__FILE__) . "/src/TimeBenchmark.php");

require_once(dirname(__FILE__) . "/src/Measure/MeasureInterface.php");
require_once(dirname(__FILE__) . "/src/Measure/CPUMeasure.php");
require_once(dirname(__FILE__) . "/src/Measure/MemoryBaseMeasure.php");
require_once(dirname(__FILE__) . "/src/ResourceBenchmark.php");
