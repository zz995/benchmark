<?php

namespace MyApp\Library;

use ReflectionClass;

class Helper {
    protected $db;

    public function __construct() {
        $this->db = \Phalcon\Di::getDefault()->getDb();
    }

    public function showTable($tableName) {
        $sql = "SHOW CREATE TABLE $tableName";
        $db = \Phalcon\Di::getDefault()->getDb();
        list($table, $created) = $db->query($sql, [])->fetch(\PDO::FETCH_NUM);
        return [
            'table' => $table,
            'created' => $created
        ];
    }

    public function showProc($procName) {
        $sql = "SHOW CREATE PROCEDURE $procName";
        $db = \Phalcon\Di::getDefault()->getDb();
        return $db->query($sql, [])->fetch(\PDO::FETCH_NUM)[2];
    }

    public function showFunc($funcName) {
        $sql = "SHOW CREATE FUNCTION $funcName";
        $db = \Phalcon\Di::getDefault()->getDb();
        return $db->query($sql, [])->fetch(\PDO::FETCH_NUM)[2];
    }

    public function showProfiling() {
        $sql = "SHOW PROFILES;";
        $db = \Phalcon\Di::getDefault()->getDb();
        $profiles = $db->query($sql, [])->fetchAll(\PDO::FETCH_ASSOC);
        return $profiles;
    }

    public function sessionStatus() {
        $sql =
            "SELECT * FROM performance_schema.session_status
                WHERE 
                    VARIABLE_NAME = 'Bytes_received'
                    OR VARIABLE_NAME = 'Bytes_sent'
                    OR VARIABLE_NAME = 'Questions'";
        $db = \Phalcon\Di::getDefault()->getDb();
        $rows1 = $db->query($sql, [])->fetchAll(\PDO::FETCH_NUM);

        $data = [];
        foreach ($rows1 as $key => list($name, $value)) {
            $name = strtolower($name);
            $pos = strpos($name, '_');
            if ($pos !== false) {
                $name = substr($name, 0, $pos - 1) . ucfirst(substr($name, $pos + 1));
            }

            $data[$name] = $value;
        }

       $data['byteReceived'] -= 344;
       $data['byteSent'] -= 106;
       $data['questions'] -= 1;

        return $data;
    }

    public function showProfileInfo($pIds) {
        if (empty($pIds)) return [];

        $sql = "SELECT QUERY_ID, SEQ, STATE, DURATION, CPU_USER, CPU_SYSTEM FROM INFORMATION_SCHEMA.PROFILING WHERE QUERY_ID IN (" . implode(',', $pIds). ")";
        $db = \Phalcon\Di::getDefault()->getDb();
        $rows = $db->query($sql, [])->fetchAll(\PDO::FETCH_ASSOC);
        $data = [];
        foreach ($rows as $row) {
            if (!isset($data[$row['query_id']])) {
                $data[$row['query_id']] = [];
            }
            $data[$row['query_id']][] = [
                'queryId' => intval($row['query_id']),
                'seq' => intval($row['seq']),
                'state' => $row['state'],
                'duration' => floatval($row['duration']),
                'cpuUser' => floatval($row['cpu_user']),
                'cpuSystem' => floatval($row['cpu_system'])
            ];
        }
        return $data;
    }

    public function countRecordTable($tableName) {
        $sql = "SELECT COUNT(*) FROM $tableName";
        $db = \Phalcon\Di::getDefault()->getDb();
        $count = $db->query($sql, [])->fetch(\PDO::FETCH_NUM)[0];
        return $count;
    }

    public function getFileContent(...$classes) {
        if (!is_array($classes)) {
            $classes = [$classes];
        }
        
        $files = array_map(function ($class){
            $reflector = new ReflectionClass($class);
            $file = $reflector->getFileName();
            return [
                'name' => basename($file),
                'content' => file_get_contents($file)
            ];
        }, $classes);

        $code = '';
        foreach ($files as $file) {
            $code .= PHP_EOL . PHP_EOL . '//' . $file['name'] . PHP_EOL . PHP_EOL;
            $code .= $file['content'];
        }

        return $code;
    }

    public function getAllMetrics($path, $sc = '') {
        return [
            'sql' => $this->getMetrics($path . '/Sql' . $sc),
            'proc' => $this->getMetrics($path . '/Proc'. $sc),
            'builder' => $this->getMetrics($path . '/Builder'. $sc),
            'orm' => $this->getMetrics($path . '/Orm' . $sc),
        ];
    }

    public function getMetrics($path) {
        $base = __DIR__ . '/../../public';

        $sumFile = '/metrics/' . 'm-' . sha1(rand(0, 10000000) . strtotime('now')) . '.xml';
        $chartFile = '/metrics/' . 'm-' . sha1(rand(0, 10000000) . strtotime('now')) . '.svg';
        $pyramidFile = '/metrics/' . 'm-' . sha1(rand(0, 10000000) . strtotime('now')) . '.svg';

        $cmd = "pdepend --summary-xml=$base$sumFile --jdepend-chart=$base$chartFile --overview-pyramid=$base$pyramidFile $path";
        shell_exec($cmd);
        $data = [
            'sum' => $sumFile,
            'chart' => $chartFile,
            'pyramid' => $pyramidFile
        ];
        /*unlink($sumFile);
        unlink($chartFile);
        unlink($pyramidFile);*/

        return $data;
    }

    public function cpuInfo() {
        $raw = file_get_contents('/proc/cpuinfo');
        $data = [];

        foreach (explode(PHP_EOL, $raw) as $row) {
            @list($name, $value) = @explode(':', $row);
            $data[trim($name)] = trim($value);
        }

        return $data;
    }

    public function memInfo() {
        $raw = file_get_contents('/proc/meminfo');
        $data = [];

        foreach (explode(PHP_EOL, $raw) as $row) {
            @list($name, $value) = @explode(':', $row);
            $data[trim($name)] = trim(explode(' ', trim($value))[0]);
        }

        return $data;
    }

    public function mysqlInfo() {
        $db = \Phalcon\Di::getDefault()->getDb();

        $data = [];
        $data['version'] = $db->query("SELECT @@version;", [])->fetch(\PDO::FETCH_NUM)[0];
        $data['query_cache_limit'] = $db->query("SELECT @@query_cache_limit;", [])->fetch(\PDO::FETCH_NUM)[0];
        $data['query_cache_size'] = $db->query("SELECT @@query_cache_size;", [])->fetch(\PDO::FETCH_NUM)[0];
        $data['profiling'] = $db->query("SELECT @@profiling;", [])->fetch(\PDO::FETCH_NUM)[0];
        $data['profiling_history_size'] = $db->query("SELECT @@profiling_history_size;", [])->fetch(\PDO::FETCH_NUM)[0];

        return $data;
    }

    public function prepareDb() {
        $db = \Phalcon\Di::getDefault()->getDb();
        $db->query("set global query_cache_limit = 0;", []);
        $db->query("set global query_cache_size = 0;", []);
        $db->query("set global profiling = 1;", []);
        $db->query("set global profiling_history_size = 1000;", []);
    }
}