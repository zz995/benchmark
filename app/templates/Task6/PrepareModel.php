<?php

namespace MyApp\Templates\Task6;

class PrepareModel {
    protected $tableCount;

    public function __construct($tableCount = 1) {
        $this->tableCount = $tableCount;
        $this->builder();
        $this->orm();
        $this->proc();
        $this->sql();
    }

    protected function sql() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task6/Table' . $i . '/Sql/SqlModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->sqlTemplate($i));
        }
    }

    protected function sqlTemplate($num) {
        $foreign = function ($t) {
            return <<<EOD
        \$foreign['table{$t}'] = \$db->query("SELECT * FROM sc6_table{$t} WHERE sc6_table1_id IN (" . implode(', ', \$ids) . ")")->fetchAll();
EOD;
        };

        $foreignPart = '';
        for($i = 0; $i < $num; $i++) {
            $foreignPart .= $foreign($i + 2) . PHP_EOL;
        }

        return <<<EOD
<?php

namespace MyApp\Models\Task6\Table{$num}\Sql;

class SqlModel {
    static public function exec(\$start, \$end) {
        \$db = \Phalcon\Di::getDefault()->getDb();
        \$data = \$db->query("SELECT * FROM sc6_table1 WHERE id BETWEEN " . intval(\$start) . " AND " . intval(\$end))->fetchAll();
        \$ids = array_map(function (\$v) { return \$v['id']; }, \$data);

        \$foreign = [];
$foreignPart

        for (\$i = 0; \$i < count(\$data); \$i++) {
            \$id = \$data[\$i]['id'];

            foreach (\$foreign as \$key => \$values) {
                if (!isset(\$data[\$i][\$key])) {
                    \$data[\$i][\$key] = [];
                }

                foreach(\$values as \$value) {
                    if (\$value['sc6_table1_id'] == \$id) {
                        \$data[\$i][\$key][] = ['id' => \$value['id'], 'field1' => \$value['field1']];
                    }
                }
            }
        }
        return \$data;
    }
}
EOD;
    }

    protected function proc() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task6/Table' . $i . '/Proc/ProcModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            $this->createProc($i);
            file_put_contents($path, $this->procTemplate($i));
        }
    }

    protected function createProc($num) {
        $param = function ($t) {
            return <<<EOD
        t{$t}.id t{$t}_id, t{$t}.field1 t{$t}_field1
EOD;
        };
        $join = function ($t) {
            return <<<EOD
        LEFT JOIN sc6_table{$t} t{$t} ON t1.id = t{$t}.sc6_table1_id
EOD;
        };

        $paramPart = '';
        $joinPart = '';
        for($i = 0; $i < $num; $i++) {
            $paramPart .= $param($i + 2) . ($i == ($num - 1) ? '' : ',' ) . PHP_EOL;
            $joinPart .= $join($i + 2) . PHP_EOL;
        }

        $sql = <<<EOD
CREATE DEFINER=`root`@`localhost` PROCEDURE `sc6_table1_t{$num}`( 
	IN `_start` INT, IN `_end` INT
)
BEGIN
	SELECT
		t1.id t1_id, t1.field1 t1_field1, t1.field2 t1_field2,
$paramPart
	FROM sc6_table1 t1
$joinPart
	WHERE t1.id BETWEEN _start AND _end; 
END
EOD;

        $db = \Phalcon\Di::getDefault()->getDb();
        $db->query("DROP procedure IF EXISTS `sc6_table1_t{$num}`;", []);
        $db->query($sql, []);
    }

    protected function procTemplate($num) {
        $mDataTable = function ($t) {
            return "'table{$t}' => []";
        };
        $trans = function ($t) {
            return <<<EOD
            \$t{$t}Id = \$row['t{$t}_id'];
            if (!isset(\$mData[\$id]['table{$t}'][\$t{$t}Id]) && isset(\$t{$t}Id)) {
                \$mData[\$id]['table{$t}'][\$t{$t}Id] = 1;
                \$mData[\$id]['table1']['table{$t}'][] = [
                    'id' => \$row['t{$t}_id'],
                    'field1' => \$row['t{$t}_field1']
                ];
            }
EOD;
        };

        $mDataTablePart = [];
        $transPart = '';
        for($i = 0; $i < $num; $i++) {
            $mDataTablePart[] = $mDataTable($i + 2);
            $transPart .= $trans($i + 2) . PHP_EOL;
        }
        $mDataTablePart = implode(', ', $mDataTablePart);

        return <<<EOD
<?php

namespace MyApp\Models\Task6\Table{$num}\Proc;

class ProcModel {
    static public function exec(\$start, \$end) {
        \$db = \Phalcon\Di::getDefault()->getDb();
        \$sql = "CALL sc6_table1_t{$num}(?, ?);";
        \$rows = \$db->query(\$sql, [\$start, \$end])->fetchAll();

        \$mData = [];
        \$data = [];
        foreach (\$rows as \$row) {
            \$id = \$row['t1_id'];
            if (!isset(\$mData[\$id])) {
                \$mData[\$id] = ['table1' => [], $mDataTablePart];
                \$mData[\$id]['table1'] = [
                    'id' => \$row['t1_id'],
                    'field1' => \$row['t1_field1'],
                    'field2' => \$row['t1_field2'],
                    $mDataTablePart
                ];
                \$data[] = &\$mData[\$id]['table1'];
            }

$transPart
        }

        return \$data;
    }
}
EOD;
    }

    protected function orm() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task6/Table' . $i . '/Orm/Table1.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->ormTemplateMainTable($i));
            for ($j = 2; $j < $i + 2; $j++) {
                $spath = __DIR__ . '/../../models/Task6/Table' . $i . '/Orm/Table' . $j . '.php';
                file_put_contents($spath, $this->ormTemplateTable($i, $j));
            }
        }
    }

    protected function ormTemplateTable($main, $cur) {
        return <<<EOL
<?php

namespace MyApp\Models\Task6\Table{$main}\Orm;

use \Phalcon\Mvc\Model;

class Table{$cur} extends Model
{
    public \$id;
    public \$field1;

    public function initialize() {
        \$this->setSource('sc6_table{$cur}');

        \$this->belongsTo(
            "sc6_table1_id",
            'MyApp\\Models\\Task6\\Table{$main}\\Orm\\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}
EOL;
    }

    protected function ormTemplateMainTable($num) {
        $hasMany = function ($curNum, $t) {
            return <<<EOD
        \$this->hasMany(
            "id",
            'MyApp\\Models\\Task6\\Table{$curNum}\\Orm\\Table{$t}',
            "sc6_table1_id",
            array(
                'alias' => 'table{$t}'
            )
        );
EOD;
        };
        $exec = function ($t) {
            return <<<EOD
            \$table1['table{$t}'] = \$table->getTable{$t}()->toArray();
EOD;
        };

        $partHasMany = '';
        $partExec = '';
        for($i = 0; $i < $num; $i++) {
            $partHasMany .= $hasMany($num, $i + 2) . PHP_EOL;
            $partExec .= $exec($i + 2) . PHP_EOL;
        }

        return <<<EOL
<?php

namespace MyApp\Models\Task6\Table{$num}\Orm;

use \Phalcon\Mvc\Model;

class Table1 extends Model
{
    public \$id;
    public \$field1;
    public \$field2;

    public function initialize() {
        \$this->setSource('sc6_table1');
$partHasMany
    }

    static public function exec(\$start, \$end) {
        \$tables = self::find(
            [
                'conditions' => 'id between :start: AND :end:',
                'bind' => [
                    'start' => \$start,
                    'end' => \$end,
                ],
            ]
        );
        
        \$data = [];
        foreach(\$tables as \$table) {
            \$table1 = \$table->toArray();
$partExec
            \$data[] = \$table1;
        }

        return \$data;
    }
}
EOL;

    }

    protected function builder() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task6/Table' . $i . '/Builder/BuilderModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->builderTemplate($i));
        }
    }

    protected function builderTemplate($num) {
        $tableConnect = function ($t) {
            return <<<EOD
        \$foreign['table{$t}'] = QueryBuilder::table('sc6_table$t')->whereIn('sc6_table1_id', \$ids)->get()->toArray();
EOD;
        };

        $part = '';
        for($i = 0; $i < $num; $i++) {
            $part .= $tableConnect($i + 2) . PHP_EOL;
        }

        return <<<EOD
<?php

namespace MyApp\Models\Task6\Table{$num}\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec(\$start, \$end) {
        \$data = QueryBuilder::table('sc6_table1')->whereBetween('id', [\$start, \$end])->get()->toArray();
        \$ids = array_map(function (\$v) { return \$v->id; }, \$data);
        
        \$foreign = []; 
$part

        for (\$i = 0; \$i < count(\$data); \$i++) {
            \$id = \$data[\$i]->id;

            foreach (\$foreign as \$key => \$values) {
                if (!isset(\$data[\$i]->{\$key})) {
                    \$data[\$i]->{\$key} = [];
                }

                foreach(\$values as \$value) {
                    if (\$value->sc6_table1_id == \$id) {
                        \$data[\$i]->{\$key}[] = ['id' => \$value->id, 'field1' => \$value->field1];
                    }
                }
            }
        }
        return \$data;
    }
}
EOD;
    }
}