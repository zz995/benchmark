<?php

namespace MyApp\Templates\Task5;

class PrepareModel {
    protected $tableCount;

    public function __construct($tableCount = 1) {
        $this->tableCount = $tableCount;
        $this->builder();
        $this->orm();
        $this->proc();
        $this->sql();
    }

    protected function sql() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task5/Table' . $i . '/Sql/SqlModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->sqlTemplate($i));
        }
    }

    protected function sqlTemplate($num) {
        $join = function ($t) {
            return <<<EOD
        \$data['table{$t}'] = \$db->query("SELECT id, field1 FROM sc5_table{$t} WHERE sc5_table1_id = " . intval(\$id))->fetchAll();
EOD;
        };

        $joinPart = '';
        for($i = 0; $i < $num; $i++) {
            $joinPart .= $join($i + 2) . PHP_EOL;
        }

        return <<<EOD
<?php

namespace MyApp\Models\Task5\Table{$num}\Sql;

class SqlModel {
    static public function exec(\$id) {
        \$db = \Phalcon\Di::getDefault()->getDb();
        \$data = \$db->query("SELECT * FROM sc5_table1 WHERE id = " . intval(\$id))->fetch();
$joinPart
        return \$data;
    }
}
EOD;
    }

    protected function proc() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task5/Table' . $i . '/Proc/ProcModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            $this->createProc($i);
            file_put_contents($path, $this->procTemplate($i));
        }
    }

    protected function createProc($num) {
        $tables = function ($t) {
            return <<<EOD
    SELECT t{$t}.id, t{$t}.field1
    FROM sc5_table{$t} t{$t}
    WHERE t{$t}.sc5_table1_id = _id;
EOD;
        };

        $tablesPart = '';
        for($i = 0; $i < $num; $i++) {
            $tablesPart .= $tables($i + 2) . PHP_EOL . PHP_EOL;
        }

        $sql = <<<EOD
CREATE DEFINER=`root`@`localhost` PROCEDURE `sc5_table1_t{$num}`( 
	IN `_id` INT
)
BEGIN
	SELECT t1.id, t1.field1, t1.field2
	FROM sc5_table1 t1
	WHERE t1.id = _id;
	
$tablesPart
END
EOD;

        $db = \Phalcon\Di::getDefault()->getDb();
        $db->query("DROP procedure IF EXISTS `sc5_table1_t{$num}`;", []);
        $db->query($sql, []);
    }

    protected function procTemplate($num) {
        $tablesPart = [];
        for($i = 0; $i < $num; $i++) {
            $tablesPart[] = '\'table' . ($i + 2) . '\'';
        }
        $tablesPart = implode(', ', $tablesPart);

        return <<<EOD
<?php

namespace MyApp\Models\Task5\Table{$num}\Proc;

class ProcModel {
    static public function exec(\$id) {
        \$db = \Phalcon\Di::getDefault()->getDb();
        \$sql = "CALL sc5_table1_t{$num}(?);";
        \$stmt = \$db->prepare(\$sql);
        \$stmt->execute(array(\$id));
        \$data = \$stmt->fetch();

        \$tables = [$tablesPart];
        \$indexTable = 0;
        for(;\$stmt->nextRowset() && \$stmt->rowCount();) {
            \$data[\$tables[\$indexTable++]] = \$stmt->fetchAll();
        }
        
        return \$data;
    }
}
EOD;
    }

    protected function orm() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task5/Table' . $i . '/Orm/Table1.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->ormTemplateMainTable($i));
            for ($j = 2; $j < $i + 2; $j++) {
                $spath = __DIR__ . '/../../models/Task5/Table' . $i . '/Orm/Table' . $j . '.php';
                file_put_contents($spath, $this->ormTemplateTable($i, $j));
            }
        }
    }

    protected function ormTemplateTable($main, $cur) {
        return <<<EOL
<?php

namespace MyApp\Models\Task5\Table{$main}\Orm;

use \Phalcon\Mvc\Model;

class Table{$cur} extends Model
{
    public \$id;
    public \$field1;

    public function initialize() {
        \$this->setSource('sc5_table{$cur}');

        \$this->belongsTo(
            "sc5_table1_id",
            'MyApp\\Models\\Task5\\Table{$main}\\Orm\\Table1',
            "id",
            array(
                'alias' => 'table1'
            )
        );
    }
}
EOL;
    }

    protected function ormTemplateMainTable($num) {
        $hasMany = function ($curNum, $t) {
            return <<<EOD
        \$this->hasMany(
            "id",
            'MyApp\\Models\\Task5\\Table{$curNum}\\Orm\\Table{$t}',
            "sc5_table1_id",
            array(
                'alias' => 'table{$t}'
            )
        );
EOD;
        };
        $exec = function ($t) {
            return <<<EOD
        \$data['table{$t}'] = \$table1->getTable{$t}()->toArray();
EOD;
        };

        $partHasMany = '';
        $partExec = '';
        for($i = 0; $i < $num; $i++) {
            $partHasMany .= $hasMany($num, $i + 2) . PHP_EOL;
            $partExec .= $exec($i + 2) . PHP_EOL;
        }

        return <<<EOL
<?php

namespace MyApp\Models\Task5\Table{$num}\Orm;

use \Phalcon\Mvc\Model;

class Table1 extends Model
{
    public \$id;
    public \$field1;
    public \$field2;

    public function initialize() {
        \$this->setSource('sc5_table1');
$partHasMany
    }

    static public function exec(\$id) {
        \$table1 = self::findFirst(\$id);
        \$data = \$table1->toArray();
$partExec
        return \$data;
    }
}
EOL;

    }

    protected function builder() {
        for ($i = 1; $i <= $this->tableCount; $i++) {
            $path = __DIR__ . '/../../models/Task5/Table' . $i . '/Builder/BuilderModel.php';
            if (file_exists($path)) continue;
            mkdir(dirname($path), 0777, true);
            file_put_contents($path, $this->builderTemplate($i));
        }
    }

    protected function builderTemplate($num) {
        $tableConnect = function ($t) {
            return <<<EOD
        \$data->table$t = QueryBuilder::table('sc5_table$t')->where('sc5_table1_id', \$id)->select('id', 'field1')->get()->toArray();
EOD;
        };

        $part = '';
        for($i = 0; $i < $num; $i++) {
            $part .= $tableConnect($i + 2) . PHP_EOL;
        }

        return <<<EOD
<?php

namespace MyApp\Models\Task5\Table{$num}\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec(\$id) {
        \$data = QueryBuilder::table('sc5_table1')->find(\$id);
$part
        return \$data;
    }
}
EOD;
    }
}