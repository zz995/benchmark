<?php

namespace MyApp\Models\Task5\Table10\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec($id) {
        $data = QueryBuilder::table('sc5_table1')->find($id);
                $data->table11 = QueryBuilder::table('sc5_table11')->where('sc5_table1_id', $id)->select('id', 'field1')->get()->toArray();
        return $data;
    }
}