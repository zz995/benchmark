<?php

namespace MyApp\Models\Task5\Table5\Builder;

use MyApp\Library\QueryBuilder;

class BuilderModel {
    static public function exec($id) {
        $data = QueryBuilder::table('sc5_table1')->find($id);
                $data->table6 = QueryBuilder::table('sc5_table6')->where('sc5_table1_id', $id)->select('id', 'field1')->get()->toArray();
        return $data;
    }
}