#include "string.h"
#include "stdio.h"
#include "mysql.h"

my_bool sc3_table1_ufunc_init(
    UDF_INIT *initid, UDF_ARGS *args, char *msg
) {
    if (args->arg_count != 3) {
    	strcpy(msg, "sc3_table1_ufunc() requires three arguments");
    	return 1;
    }
    if (
        args->arg_type[0] != INT_RESULT
        || args->arg_type[1] != INT_RESULT
        || args->arg_type[2] != INT_RESULT
    ) {
    	strcpy(msg, "sc3_table1_ufunc() requires three arguments an integer type");
    	return 1;
    }
    return 0;
}
my_bool sc3_table1_ufunc(
    UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error
) {
    long long field1, field2, field3;
    field1 = *((long long*) args->args[0]);
    field2 = *((long long*) args->args[1]);
    field3 = *((long long*) args->args[2]);

    my_bool correct = field1 == 10 && field2 > 30 && field3 < 50;
    return correct;
}