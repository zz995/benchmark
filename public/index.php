<?php

use Phalcon\Mvc\Application;
use Illuminate\Events\Dispatcher as IlluminateDispatcher;
use Illuminate\Container\Container as IlluminateContainer;

try {
    $config = include __DIR__ . "/../app/config/config.php";
    if (isset($config->application->env) && $config->application->env == 'debug') {
        error_reporting(E_ALL);
        function errHandle($errNo, $errStr, $errFile, $errLine) {
            if ( error_reporting() == 0 ) {
                return;
            }
            $msg = "$errStr in $errFile on line $errLine";
            $msg .= '';
        }
        set_error_handler('errHandle');
    } else {
        error_reporting(E_ALL & ~E_NOTICE);
    }

    require __DIR__ . '/../vendor/autoload.php';
    include __DIR__ . "/../app/config/loader.php";
    include __DIR__ . "/../app/config/services.php";

    $capsule = new \MyApp\Library\QueryBuilder();
    $capsule->addConnection([
        'driver' => 'mysql',
        'host' => $config->database->host,
        'database' => $config->database->dbname,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'charset' => $config->database->charset,
        'options' => [PDO::ATTR_CASE => PDO::CASE_LOWER, PDO::ATTR_PERSISTENT => FALSE, PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC],
        'prefix' => '',
    ]);
    $capsule->getConnection()->setPdo($di->getDb());
    $capsule->setEventDispatcher(new IlluminateDispatcher(new IlluminateContainer));
    $capsule->setAsGlobal();

    $application = new Application($di);
    echo $application->handle()->getContent();
} catch (\Exception $err) {
    error_log(
        print_r(
            $err->getMessage(), true
        )
    );

    http_response_code('500');
    echo "<div style='text-align: center'><h2>500</h2><h2>Ошибка сервера</h2></div>";
} catch (\Error $err) {
    error_log(
        print_r(
            $err->getMessage() . ' in ' . $err->getFile() . ' on line ' . $err->getLine(), true
        )
    );

    http_response_code('400');
    echo "<div style='text-align: center'><h2>400</h2><h2>Неправильный запрос</h2></div>";
}