'use strict';

$(function () {
    var measuresHtml = new MeasuresHtml();

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        var recordsCount = +$("[name='records_count']").val();

        var measures = new Measures();
        measures.load(count, {'records_count': recordsCount}, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});