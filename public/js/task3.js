'use strict';

$(function () {
    var measuresHtml = new MeasuresHtml();

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        var sqlType = +$("[name='sql_type']").val();
        var procType = +$("[name='proc_type']").val();
        var builderType = +$("[name='builder_type']").val();
        var ormType = +$("[name='orm_type']").val();

        var measures = new Measures();
        measures.load(count, {
            'sql_type': sqlType,
            'proc_type': procType,
            'builder_type': builderType,
            'orm_type': ormType
        }, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});