'use strict';

function LoadDataBase(task, repeat, params, cb, iterCb) {
    this.paramsIndex = [];
    this.paramsKeys = [];
    this.paramsValues = [];

    this.repeat = repeat;

    var $this = this;
    var count = 1;
    params.forEach(function (v) {
       if (v.values.length) {
           $this.paramsIndex.push(0);
           $this.paramsKeys.push(v.key);
           $this.paramsValues.push(v.values);
           count *= v.values.length;
       }
    });

    this.params = params;

    LoadData.apply(this, [task, count, {}, cb, iterCb]);
}

LoadDataBase.prototype = Object.create(LoadData.prototype);

LoadDataBase.prototype.ajax = function () {
    var $this = this;
    this.urlList = [''];
    var param = this.getParam();

    $.ajax({
        'url': '/' + this.task + '/base?' + $.param(param)
    }).done(function (jsonRes) {
        $this.result(jsonRes);
    });
};

LoadDataBase.prototype.getParam = function () {
    var param = {};
    param['repeat'] = this.repeat;

    var transfer = 1;
    for(var l = this.paramsKeys.length; l--;) {
        var key = this.paramsKeys[l];
        var values = this.paramsValues[l];
        var index = this.paramsIndex[l];

        param[key] = values[index];

        if (transfer) {
            index += 1;
            index %= values.length;
            this.paramsIndex[l] = index;
            transfer = index == 0 ? 1 : 0;
        }
    }

    return param;
};