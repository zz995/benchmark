'use strict';

function LoadDataRegression(data, paramName, cb, iterCb, errCb) {
    this.analysData = data;
    this.paramName = paramName;
    this.errCb = errCb;

    LoadData.apply(this, ['', 1, {}, cb, iterCb]);
}
LoadDataRegression.prototype = Object.create(LoadData.prototype);
LoadDataRegression.prototype.ajax = function (url) {
    var $this = this;
    $.ajax({
        'url': '/regression/multivariate',
        'method': 'POST',
        'data': JSON.stringify(this.prepareData(url))
    }).done(function (jsonRes) {
        $this.result(jsonRes);
    }).fail(function () {
        $this.errCb && $this.errCb();
    });
};
LoadDataRegression.prototype.prepareData = function (url) {
    var x = [];
    var y = [];
    var paramName = this.paramName;

    this.analysData.forEach(function (v) {
        if (v.avg[url] !== null) {
            x.push(
                paramName.map(function (name) {
                    return v.param[name];
                })
            );
            y.push([v.avg[url]]);
        }
    });

    var data = {
        x: x,
        y: y
    };

    return data;
};
