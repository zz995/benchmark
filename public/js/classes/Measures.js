'use strict';

function Measures() {
    this.task = window.TASK || 'task1';

    this.sql = [];
    this.proc = [];
    this.builder = [];
    this.orm = [];

    this.queries = undefined;
    this.sessionStat = undefined;

    this.count = 0;
}

Measures.DB_ACCESS_SQL = 1;
Measures.DB_ACCESS_PROC = 2;
Measures.DB_ACCESS_BUILDER = 3;
Measures.DB_ACCESS_ORM = 4;

Measures.SORT_TIME = 1;
Measures.SORT_RESOURCE = 2;

Measures.RESOURCE_MEMORY = 1;
Measures.RESOURCE_CPU = 2;

Measures.STATISTIC_MIN = 1;
Measures.STATISTIC_MEDIAN = 2;
Measures.STATISTIC_MAX = 3;

Measures.prototype.load = function (count, param, cb, iterCb) {
    this.count = count;

    this.sql = [];
    this.proc = [];
    this.builder = [];
    this.orm = [];

    var $this = this;
    new LoadData(this.task, this.count, param, function (data) {
        $this.sql = data.sql;
        $this.proc = data.proc;
        $this.builder = data.builder;
        $this.orm = data.orm;

        new LoadDataQuery($this.task, param, function (data) {
            $this.queries = {};
            $this.queries.sql = data.sql[0].queries;
            $this.queries.proc = data.proc[0].queries;
            $this.queries.builder = data.builder[0].queries;
            $this.queries.orm = data.orm[0].queries;

            $this.sessionStat = {};
            $this.sessionStat.sql = data.sql[0].stat;
            $this.sessionStat.proc = data.proc[0].stat;
            $this.sessionStat.builder = data.builder[0].stat;
            $this.sessionStat.orm = data.orm[0].stat;

            cb();
        });
    }, iterCb);
};

Measures.prototype.hasError = function (type) {
    if (type == Measures.DB_ACCESS_SQL) {
        return this.sqlHasError();
    };
    if (type == Measures.DB_ACCESS_PROC) {
        return this.procHasError();
    };
    if (type == Measures.DB_ACCESS_BUILDER) {
        return this.builderHasError();
    };
    if (type == Measures.DB_ACCESS_ORM) {
        return this.ormHasError();
    };
};

Measures.prototype.sqlHasError = function () {
    return this.sql && this.sql.length && this.sql[0].error == 1
};

Measures.prototype.procHasError = function () {
    return this.proc && this.proc.length && this.proc[0].error == 1
};

Measures.prototype.builderHasError = function () {
    return this.builder && this.builder.length && this.builder[0].error == 1
};

Measures.prototype.ormHasError = function () {
    return this.orm && this.orm.length && this.orm[0].error == 1
};

Measures.prototype.getSessionStat = function (resourceType) {
    if (resourceType == Measures.DB_ACCESS_SQL) {
        return this.sessionStat.sql;
    }
    if (resourceType == Measures.DB_ACCESS_PROC) {
        return this.sessionStat.proc;
    }
    if (resourceType == Measures.DB_ACCESS_BUILDER) {
        return this.sessionStat.builder;
    }
    if (resourceType == Measures.DB_ACCESS_ORM) {
        return this.sessionStat.orm;
    }
};

Measures.prototype.getQueries = function (resourceType) {
    if (resourceType == Measures.DB_ACCESS_SQL) {
        return this.queries.sql;
    }
    if (resourceType == Measures.DB_ACCESS_PROC) {
        return this.queries.proc;
    }
    if (resourceType == Measures.DB_ACCESS_BUILDER) {
        return this.queries.builder;
    }
    if (resourceType == Measures.DB_ACCESS_ORM) {
        return this.queries.orm;
    }
};

Measures.prototype.scatterData = function (resourceType) {
    var data = [];
    var $this = this;

    if (!this.sqlHasError()) {
        var sqlData = [];
        this.sql.forEach(function (v) {
            sqlData = sqlData.concat($this.prepareChartData($this.getMemoryStamp(v)));
        });
        data.push({
            data: sqlData,
            access: Measures.DB_ACCESS_SQL
        });
    }

    if (!this.procHasError()) {
        var procData = [];
        this.proc.forEach(function (v) {
            procData = procData.concat($this.prepareChartData($this.getMemoryStamp(v)));
        });
        data.push({
            data: procData,
            access: Measures.DB_ACCESS_PROC
        });
    }

    if (!this.builderHasError()) {
        var builderData = [];
        this.builder.forEach(function (v) {
            builderData = builderData.concat($this.prepareChartData($this.getMemoryStamp(v)));
        });
        data.push({
            data: builderData,
            access: Measures.DB_ACCESS_BUILDER
        });
    }

    if (!this.ormHasError()) {
        var ormData = [];
        this.orm.forEach(function (v) {
            ormData = ormData.concat($this.prepareChartData($this.getMemoryStamp(v)));
        });
        data.push({
            data: ormData,
            access: Measures.DB_ACCESS_ORM
        });
    }

    return data;
};

Measures.prototype.dataMedian = function (access, sortType, resourceType, statisticType) {
    var data = undefined;
    switch (access) {
        case Measures.DB_ACCESS_SQL:
            data = this.sql;
            break;
        case Measures.DB_ACCESS_PROC:
            data = this.proc;
            break;
        case Measures.DB_ACCESS_BUILDER:
            data = this.builder;
            break;
        case Measures.DB_ACCESS_ORM:
            data = this.orm;
            break;
    }
    if (data === undefined || !data.length) {
        return undefined;
    }

    var sortFunc = undefined;
    if (sortType == Measures.SORT_TIME) {
        sortFunc = this.timeSort.bind(this);
    } else if (sortType == Measures.SORT_RESOURCE && resourceType == Measures.RESOURCE_MEMORY) {
        sortFunc = this.memorySort.bind(this);
    } else if (sortType == Measures.SORT_RESOURCE && resourceType == Measures.RESOURCE_CPU) {
        sortFunc = this.cpuSort.bind(this);
    }
    if (sortFunc == undefined) {
        return undefined;
    }

    var stampFunc = undefined;
    if (resourceType == Measures.RESOURCE_MEMORY) {
        stampFunc = this.getMemoryStamp.bind(this);
    } else if (resourceType == Measures.RESOURCE_CPU) {
        stampFunc = this.getCpuStamp.bind(this);
    }
    if (stampFunc == undefined) {
        return undefined;
    }

    data = JSON.parse(JSON.stringify(data));
    data.sort(sortFunc);

    var index = 0;
    if (statisticType == Measures.STATISTIC_MIN) {
        index = 0;
    } else if (statisticType == Measures.STATISTIC_MEDIAN) {
        index = Math.round(data.length / 2) - 1;
    } else if (statisticType == Measures.STATISTIC_MAX) {
        index = data.length - 1;
    }

    return this.prepareChartData(stampFunc(data[index]));
};

Measures.prototype.timeSort = function (a, b) {
    return a.exec.time - b.exec.time;
};

Measures.prototype.memorySort = function (a, b) {
    return a.memory.used.max[1] - b.memory.used.max[1];
};

Measures.prototype.cpuSort = function (a, b) {
    return a.cpu.total[1] - b.cpu.total[1];
};

Measures.prototype.getCpuStamp = function (data) {
    return data['cpu']['stamp'];
};

Measures.prototype.hasAnyCpuStamp = function () {
    return (
        this.hasSqlCpuStamp()
        || this.hasProcCpuStamp()
        || this.hasBuilderCpuStamp()
        || this.hasOrmCpuStamp()
    );
};

Measures.prototype.hasSqlCpuStamp = function () {
    return this.hasCpuStamp(this.sql);
};

Measures.prototype.hasProcCpuStamp = function () {
    return this.hasCpuStamp(this.proc);
};

Measures.prototype.hasBuilderCpuStamp = function () {
    return this.hasCpuStamp(this.builder);
};

Measures.prototype.hasOrmCpuStamp = function () {
    return this.hasCpuStamp(this.orm);
};

Measures.prototype.hasCpuStamp = function (data) {
    return data.every(function (v) {
       return !!v.cpu.stamp.length;
    });
};

Measures.prototype.getMemoryStamp = function (data) {
    return data['memory']['used']['stamp'];
};

Measures.prototype.prepareChartData = function (stamps) {
    return stamps.map(function (v) {
        return {x: v[0], y: v[1]};
    });
};

Measures.prototype.getTimeScale = function () {
    return this.sql[0]['exec']['scale'][1];
};

Measures.prototype.getMemoryScaleMemory = function () {
    return this.sql[0]['memory']['scale'][1][1];
};

Measures.prototype.getMemoryScaleTime = function () {
    return this.sql[0]['memory']['scale'][0][1];
};

Measures.prototype.getCpuScaleCpu = function () {
    return this.sql[0]['cpu']['scale'][1][1];
};

Measures.prototype.getCpuScaleTime = function () {
    return this.sql[0]['cpu']['scale'][0][1];
};

Measures.prototype.getData = function (type) {
    var mapFunc = {
        'MaxTime': this.getMaxTimeByData,
        'MinTime': this.getMinTimeByData,
        'AvgTime': this.getAvgTimeByData,
        'TotalTime': this.getTotalTimeByData,
        'MaxMemory': this.getMaxMemoryByData,
        'MinMemory': this.getMinMemoryByData,
        'AvgMemory': this.getAvgMemoryByData,
        'MaxCpu': this.getMaxCpuByData,
        'MinCpu': this.getMinCpuByData,
        'AvgCpu': this.getAvgCpuByData
    };

    return {
        'sql': mapFunc[type](this.sql)[0],
        'proc': mapFunc[type](this.proc)[0],
        'builder': mapFunc[type](this.builder)[0],
        'orm': mapFunc[type](this.orm)[0]
    };
};

Measures.prototype.getMaxCpu = function () {
    return this.getData('MaxCpu');
};

Measures.prototype.getMinCpu = function () {
    return this.getData('MinCpu');
};

Measures.prototype.getAvgCpu = function () {
    return this.getData('AvgCpu');
};

Measures.prototype.getMaxMemory = function () {
    return this.getData('MaxMemory');
};

Measures.prototype.getMinMemory = function () {
    return this.getData('MinMemory');
};

Measures.prototype.getAvgMemory = function () {
    return this.getData('AvgMemory');
};

Measures.prototype.getMaxTime = function () {
    return this.getData('MaxTime');
};

Measures.prototype.getMinTime = function () {
    return this.getData('MinTime');
};

Measures.prototype.getAvgTime = function () {
    return this.getData('AvgTime');
};

Measures.prototype.getTotalTime = function () {
    return this.getData('TotalTime');
};

Measures.prototype.getAvgCpuByData = function (data) {
    if (!data.length) return [0, undefined];

    var count = data.length;
    var sum = data.reduce(function (a, v) {
        return a + v.cpu.total[1];
    }, 0);
    var avg = Math.round(sum / count);

    return [avg, undefined];
};

Measures.prototype.getMinCpuByData = function (data) {
    if (!data.length) return [0, undefined];

    var min = Number.MAX_VALUE;
    var index = 0;
    data.forEach(function (v, i) {
        if (min > v.cpu.total[1]) {
            min = v.cpu.total[1];
            index = i;
        }
    });

    return [min, index];
};

Measures.prototype.getMaxCpuByData = function (data) {
    if (!data.length) return [0, undefined];

    var max = 0;
    var index = 0;
    data.forEach(function (v, i) {
        if (max < v.cpu.total[1]) {
            max = v.cpu.total[1];
            index = i;
        }
    });

    return [max, index];
};

Measures.prototype.getAvgMemoryByData = function (data) {
    if (!data.length) return [0, undefined];

    var count = data.length;
    var sum = data.reduce(function (a, v) {
        return a + v.memory.used.max[1];
    }, 0);
    var avg = Math.round(sum / count);

    return [avg, undefined];
};

Measures.prototype.getMinMemoryByData = function (data) {
    if (!data.length) return [0, undefined];

    var min = Number.MAX_VALUE;
    var index = 0;
    data.forEach(function (v, i) {
        if (min > v.memory.used.max[1]) {
            min = v.memory.used.max[1];
            index = i;
        }
    });

    return [min, index];
};

Measures.prototype.getMaxMemoryByData = function (data) {
    if (!data.length) return [0, undefined];

    var max = 0;
    var index = 0;
    data.forEach(function (v, i) {
        if (max < v.memory.used.max[1]) {
            max = v.memory.used.max[1];
            index = i;
        }
    });

    return [max, index];
};

Measures.prototype.getTotalTimeByData = function (data) {
    if (!data.length) return [0, undefined];

    var sum = data.reduce(function (a, v) {
        return a + v.exec.time
    }, 0);

    return [sum, undefined];
};

Measures.prototype.getAvgTimeByData = function (data) {
    if (!data.length) return [0, undefined];

    var count = data.length;
    var sum = data.reduce(function (a, v) {
        return a + v.exec.time
    }, 0);
    var avg = Math.round(sum / count);

    return [avg, undefined];
};

Measures.prototype.getMinTimeByData = function (data) {
    if (!data.length) return [0, undefined];

    var min = Number.MAX_VALUE;
    var index = 0;
    data.forEach(function (v, i) {
        if (min > v.exec.time) {
            min = v.exec.time;
            index = i;
        }
    });

    return [min, index];
};

Measures.prototype.getMaxTimeByData = function (data) {
    if (!data.length) return [0, undefined];

    var max = 0;
    var index = 0;
    data.forEach(function (v, i) {
        if (max < v.exec.time) {
            max = v.exec.time;
            index = i;
        }
    });

    return [max, index];
};