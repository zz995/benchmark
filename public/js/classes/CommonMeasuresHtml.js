'use strict';

function CommonMeasuresHtml(titleParam) {
    this.resultLoadElem = $(".result-load");
    this.resultElem = $(".result");
    this.buttonSend = $("#send");
    this.progressBar = $(".progress");
    var titleParamMap = {};
    titleParam.forEach(function (v) {
        titleParamMap[v[0]] = v[1];
    });
    this.titleParamMap = titleParamMap;
    this.titleMap = [
        ['sql', 'SQL'],
        ['proc', 'Збережена процедура'],
        ['builder', 'Будівальник запитів'],
        ['orm', 'ORM']
    ];

    this.mainTable = {
        'head': titleParam.concat(this.titleMap)
    };

    this.measures = undefined;
}

CommonMeasuresHtml.prototype.startLoad = function () {
    this.resultElem.addClass('uk-hidden');
    this.resultLoadElem.removeClass('uk-hidden');
    this.buttonSend.prop('disabled', true);

    this.clearProgress();
};

CommonMeasuresHtml.prototype.endLoad = function () {
    this.buttonSend.prop('disabled', false);
    this.resultLoadElem.addClass('uk-hidden');
    this.resultElem.empty().removeClass('uk-hidden');
};

CommonMeasuresHtml.prototype.set = function (measures) {
    this.measures = measures;

    this.endLoad();
    this.renderResult();
};

CommonMeasuresHtml.prototype.renderResult = function () {
    var html = '';

    html += this.getMainTable();
    html += '<div class="uk-width-1-1 uk-margin-large-top"><span class="uk-h2">Регресія<span></span><hr></div>';
    html += this.getRegressionHtml();

    var $this = this;
    setTimeout(function () {
        $this.chartAllRender();
    }, 0);

    this.resultElem.html(html);
};

CommonMeasuresHtml.prototype.progress = function (url, curCount, totalCount) {
    this.progressBar.css('width', Math.round(curCount / totalCount * 100) + '%');
    this.progressBar.text(~~(curCount / totalCount * 100) + ' %');
};

CommonMeasuresHtml.prototype.clearProgress = function () {
    var newProgressBar = this.progressBar.clone(true);
    newProgressBar.css('width', 0).text('');
    newProgressBar.insertAfter(this.progressBar);
    this.progressBar.remove();
    this.progressBar = newProgressBar;
};

CommonMeasuresHtml.prototype.getRegressionHtml = function () {
    var html = '';
    var regression = this.measures.regression;
    var correctParamNames = this.measures.correctParamNames;
    var titleParamMap = this.titleParamMap;

    var $this = this;

    if (regression === undefined) {
        return '<div class="uk-grid"><div class="uk-width-1-1">Неможливо здійснити регресійний аналіз</div></div>';
    }

    html += '<div class="uk-grid">';
    html += this.titleMap.map(function (v) {
        var html = '';

        html +=
            '<div class="uk-width-1-2 uk-margin-large-bottom">'
                + '<p class="uk-h3">' + v[1] + '</p>'
                + '<p><strong>R<sup>2</sup>:</strong> ' + regression[v[0]].rquare + '</p>'
                + '<p><strong>F:</strong> ' + regression[v[0]].f + '</p>'
                + '<p><strong>Коефіцієнти:</strong> '
                    + regression[v[0]].coefficients.map(function (c) {
                        return ~~(c[0] * 100) / 100;
                    }).join(', ')
                + '</p>'
                + '<p><strong>Стандартна помилка:</strong> '
                    + regression[v[0]].stderrors.map(function (e) {
                        return ~~(e * 100) / 100;
                    }).join(', ')
                + '</p>'
                + '<p><strong>P-заначення:</strong> '
                    + regression[v[0]].pvalues.map(function (e) {
                        return ~~(e * 100) / 100;
                    }).join(', ')
                + '</p>'
                + '<p>'
                    + 'y = '
                    + regression[v[0]].coefficients.map(function (c, i) {
                        var val = ~~(c[0] * 100) / 100;
                        if (val > 0 && i) {
                            val = '+ ' + val;
                        }
                        if (i) {
                            val += 'x' + '<sub>' + i + '</sub>';
                        }
                        return val;
                    }).join(' ')
                    + '<br>'
                    + 'де '
                    + correctParamNames.map(function (name, i) {
                        return 'x' + '<sub>' + (i + 1) + '</sub> - ' + titleParamMap[name].toLowerCase();
                    }).join(', ')
                + '</p>'
            + '</div>';

        return html;
    }).join('');

    html += correctParamNames.map(function (name) {
        return (
            '<div class="uk-width-1-1 uk-margin-large-bottom">'
                + $this.chartHtml(name)
            + '</div>'
        );
    }).join('');

    html += '</div>';

    return html;
};

CommonMeasuresHtml.prototype.chartHtml = function (type) {
    var html = '';

    html += '<div class="uk-margin-top chart-content chart-' + type + '"><canvas></canvas></div>';

    return html;
};

CommonMeasuresHtml.prototype.chartAllRender = function () {
    var correctParamNames = this.measures.correctParamNames;
    var titleParamMap = this.titleParamMap;

    var $this = this;
    correctParamNames.forEach(function (name, i) {
        var ctx = $('.chart-' + name).find('canvas').get(0).getContext('2d');
        var data = $this.titleMap.map(function (v) {
            return {
                access: v[0],
                data: $this.chartDataRender(v[0], name, i)
            };
        });

        $this.chartRender(ctx, data, titleParamMap[name], 'Час виконання');
    })
};

CommonMeasuresHtml.prototype.chartStyle = function (access) {
    if (access == 'sql') {
        return {
            label: 'SQL',
            color: "rgb(255, 99, 132)",
            form: 'rect'
        }
    }
    if (access == 'proc') {
        return {
            label: 'Збережена процедура',
            color: "rgb(54, 162, 235)",
            form: 'triangle'
        }
    }
    if (access == 'builder') {
        return {
            label: 'Будівальник запитів',
            color: "rgb(75, 192, 192)",
            form: 'cross'
        }
    }
    if (access == 'orm') {
        return {
            label: 'ORM',
            color: "rgb(255, 205, 86)",
            form: 'circle'
        }
    }
    return undefined;
};

CommonMeasuresHtml.prototype.chartDataRender = function (type, name, index) {
    var data = [];

    var coefficients = this.measures.regression[type].coefficients;
    var values = this.measures.getValues(name);

    var min = Math.min.apply(null, values);
    var max = Math.max.apply(null, values);

    data.push({
        x: min,
        y: coefficients[0][0] + coefficients[index + 1][0] * min
    });

    data.push({
        x: max,
        y: coefficients[0][0] + coefficients[index + 1][0] * max
    });
    /*var h = Math.round((max - min) / 10) || 1;

    for(var j = min; j < max + h; j += h) {
        var x = j;
        var y = coefficients[0][0] + coefficients[index + 1][0] * x;
        data.push({x: x, y: y});
    }*/

    return data;
};

CommonMeasuresHtml.prototype.chartRender = function (ctx, data, xLabelString, yLabelString) {
    var $this = this;
    var datasets = data.map(function (v) {
        return {
            label: $this.chartStyle(v.access).label,
            data: v.data,
            backgroundColor: $this.chartStyle(v.access).color,
            borderColor: $this.chartStyle(v.access).color,
            pointStyle: $this.chartStyle(v.access).form,
            borderWidth: 1,
            fill: false
        };
    });
    new Chart(ctx, {
        type: 'line',
        data: {
            datasets: datasets
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    scaleLabel: {
                        display: true,
                        labelString: xLabelString
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: yLabelString
                    }
                }]
            }
        }
    });
};

CommonMeasuresHtml.prototype.getMainTable = function () {
    var html = '';

    html += this.getTableHeadHtml();
    html += this.getTableMainHtml();

    return html;
};

CommonMeasuresHtml.prototype.getTableMainHtml = function () {
    var html = '';
    var rowCount = this.mainTable.head.length;
    if (rowCount > 10) {
        rowCount = 10;
    }

    var getRowHtml = function (data, title) {
        var value = '';
        if (data['param'][title[0]] !== undefined) {
            value = data['param'][title[0]];
        } else if (data['avg'][title[0]] !== undefined) {
            if (data['avg'][title[0]] == null) {
                value = '-';
            } else {
                value = data['avg'][title[0]] + ' ' + data['avgScale'][1];
            }
        }

        return (
            '<div class="uk-width-medium-1-' + rowCount + '">'
                + value
            + '</div>'
        );
    };

    for(var i = 0; i < this.measures.data.length; i++) {
        var data = this.measures.data[i];
        var getParamRowHtml = getRowHtml.bind(null, data);
        html += '<div class="table__main">'
                + '<div class="uk-grid">'
                    + this.mainTable.head.map(getParamRowHtml).join('')
                + '</div>'
            + '</div>';
    }


    return html;
};

CommonMeasuresHtml.prototype.getTableHeadHtml = function () {
    var html = '';
    var rowCount = this.mainTable.head.length;
    if (rowCount > 10) {
        rowCount = 10;
    }
    rowCount = 7;
    var getRowHtml = function (title) {
        return (
            '<div class="uk-width-medium-1-' + rowCount + '">'
                + title[1]
            + '</div>'
        );
    };

    html = '<div class="table__head">'
            + '<div class="uk-grid">'
                + this.mainTable.head.map(getRowHtml).join('')
            + '</div>'
        + '</div>';

    return html;
};