'use strict';

function LoadDataQuery(task, param, cb, iterCb) {
    LoadData.apply(this, [task, 1, param, cb, iterCb]);
}
LoadDataQuery.prototype = Object.create(LoadData.prototype);
LoadDataQuery.prototype.ajax = function (url) {
    var $this = this;
    $.ajax({
        'url': '/' + this.task + '/queries/' + url + '?' + $.param(this.param)
    }).done(function (jsonRes) {
        $this.result(jsonRes);
    });
};