'use strict';

function CommonMeasures(params) {
    this.task = window.TASK || 'task5';

    this.data = undefined;
    this.regression = undefined;
    this.paramNames = undefined;
    this.correctParamNames = undefined;
    this.paramReq = params;
}

CommonMeasures.prototype.getValues = function (name) {
    var values = undefined;

    this.paramReq.forEach(function (v) {
        if (v.key == name) {
            values = v.values;
        }
    });

    return values;
};

CommonMeasures.prototype.load = function (count, cb, iterCb) {
    var $this = this;
    var params = this.paramReq;
    new LoadDataBase(this.task, count, params, function (data) {
        $this.data = Object.values(data)[0];
        $this.paramNames = params.map(function (v) {
            return v.key;
        });
        $this.correctParamNames = params.filter(function (v) {
            return v.values.length > 1;
        }).map(function (v) {
            return v.key;
        });
        new LoadDataRegression($this.data, $this.correctParamNames, function (data) {
            $this.regression = {};
            for(var key in data) {
                if (!data.hasOwnProperty(key)) continue;

                $this.regression[key] = data[key][0];
            }
            cb();
        }, undefined, function () {
            cb();
        });
    }, iterCb);
};