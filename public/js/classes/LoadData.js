'use strict';

function LoadData(task, count, param, cb, iterCb) {
    this.data = {};
    this.task = task;
    this.urlList = ['sql', 'proc', 'builder', 'orm'];
    this.curUrlIndex = 0;
    this.curCount = 0;
    this.count = count || 1;
    this.param = param;
    this.cb = cb;
    this.iterCb = iterCb;

    this.load();
}
LoadData.prototype.load = function () {
    if (this.curCount < this.count) {
        this.curCount++;
    } else if (this.curUrlIndex < (this.urlList.length - 1)) {
        this.curUrlIndex++;
        this.curCount = 1;
    } else {
        return this.cb(this.data);
    }

    this.ajax(this.urlList[this.curUrlIndex]);
};
LoadData.prototype.result = function (json) {
    var url = this.urlList[this.curUrlIndex];
    if (this.data[url] === undefined) {
        this.data[url] = [];
    }

    this.data[url].push(json);
    this.iterCb && this.iterCb(this.urlList[this.curUrlIndex], this.curCount, this.count);
    this.load();
};
LoadData.prototype.ajax = function (url) {
    var $this = this;
    $.ajax({
        'url': '/' + this.task + '/benchmark/' + url + '?' + $.param(this.param)
    }).done(function (jsonRes) {
        $this.result(jsonRes);
    });
};