'use strict';

function MeasuresHtml() {
    this.resultLoadElem = $(".result-load");
    this.resultElem = $(".result");
    this.buttonSend = $("#send");

    this.measures = undefined;
    this.memoryChart = undefined;
}

MeasuresHtml.prototype.startLoad = function () {
    this.resultElem.addClass('uk-hidden');
    this.resultLoadElem.removeClass('uk-hidden');
    this.buttonSend.prop('disabled', true);

    this.clearProgress();
};

MeasuresHtml.prototype.endLoad = function () {
    this.buttonSend.prop('disabled', false);
    this.resultLoadElem.addClass('uk-hidden');
    this.resultElem.empty().removeClass('uk-hidden');
};

MeasuresHtml.prototype.set = function (measures) {
    this.measures = measures;

    this.endLoad();
    this.renderResult();
};

MeasuresHtml.prototype.renderResult = function () {
    var html = '';

    html += this.getTableHtml();

    html += '<div class="uk-width-1-1 uk-margin-large-top"><span class="uk-h2">Запити до бази даних<span></span><hr></div>';
    html += this.getSessionStatHtml();
    html += this.getQueriesHtml();

    html += '<div class="uk-width-1-1 uk-margin-large-top"><span class="uk-h2">Оперативна пам\'ять<span></span><hr></div>';
    html += this.getMemoryChartSelectData();
    html += '<div class="uk-width-1-1 memory-chart">' + this.memoryChartHtml() + '</div>';
    html += '<div class="uk-width-1-1 uk-margin-large-top memory-scatter-chart">' + this.memoryScatterChartHtml() + '</div>';

    if (this.measures.hasAnyCpuStamp()) {
        html += '<div class="uk-width-1-1 uk-margin-large-top"><span class="uk-h2">Завантаженість процесора<span></span><hr></div>';
        html += this.getCpuChartSelectData();
        html += '<div class="uk-width-1-1 cpu-chart">' + this.cpuChartHtml() + '</div>';
    }

    var $this = this;
    setTimeout(function () {
        $this.memoryScatterChartRender();
        $this.memoryBaseChartRender();
        $this.memoryBaseChartChangeHandler();

        if ($this.measures.hasAnyCpuStamp()) {
            $this.cpuBaseChartRender();
            $this.cpuBaseChartChangeHandler();
        }

        $this.queriesChangeHandler();

        $this.memoryBaseDownloadHandler();
    }, 0);

    this.resultElem.html(html);
};

MeasuresHtml.prototype.memoryBaseDownloadHandler = function () {
    var $this = this;

    $(".download-data-memory-base").on('click', function (e) {
        var paramSelect = $this.memorySelect();

        var datasets = paramSelect.map(function (v) {
            return {
                label: $this.chartStyle(v.access).label,
                data: $this.measures.dataMedian(v.access, v.sort, v.resource, v.statistic)
            };
        });

        var csv = [];
        var csvHeader = [];
        var csvHeader2 = [];
        datasets.forEach(function (v, i) {
            csvHeader.push(v.label);
            csvHeader.push('');

            csvHeader2.push('Час виконання (' + $this.measures.getMemoryScaleTime() + ')');
            csvHeader2.push('Оперативна пам\'ять (' + $this.measures.getMemoryScaleMemory() + ')');
        });

        csv.push(csvHeader.join(','));
        csv.push(csvHeader2.join(','));

        var maxLength = 0;
        datasets.forEach(function (v) {
            if (maxLength < v.data.length) {
                maxLength = v.data.length;
            }
        });
        for (var i = 0; i < maxLength; i++) {
            var csvMain = [];
            datasets.forEach(function (v) {
                if (v.data[i] !== undefined ) {
                    csvMain.push(v.data[i].x);
                    csvMain.push(v.data[i].y);
                } else {
                    csvMain.push('');
                    csvMain.push('');
                }
            });
            csv.push(csvMain);
        }

        download('data-memory-base.csv', csv.join('\n'));
    });
};

MeasuresHtml.prototype.cpuSelect = function () {
    var cpuSelect = $(".chart-cpu-select select");
    return this.select(cpuSelect, Measures.RESOURCE_CPU);
};

MeasuresHtml.prototype.memorySelect = function () {
    var memorySelect = $(".chart-memory-select select");
    return this.select(memorySelect, Measures.RESOURCE_MEMORY);
};

MeasuresHtml.prototype.select = function (elem, resource) {
    var paramSelect = [].map.call(elem, function (e) {
        var $e = $(e);
        var val = $e.val();
        var option = $e.find("option[value='" + val + "']");
        return {
            'access': option.data('access'),
            'sort': option.data('sort'),
            'statistic': option.data('statistic'),
            'resource': resource
        };
    });

    return paramSelect;
};

MeasuresHtml.prototype.chartStyle = function (access) {
    if (access == Measures.DB_ACCESS_SQL) {
        return {
            label: 'SQL',
            color: "rgb(255, 99, 132)",
            form: 'rect'
        }
    }
    if (access == Measures.DB_ACCESS_PROC) {
        return {
            label: 'Збережена процедура',
            color: "rgb(54, 162, 235)",
            form: 'triangle'
        }
    }
    if (access == Measures.DB_ACCESS_BUILDER) {
        return {
            label: 'Будівальник запитів',
            color: "rgb(75, 192, 192)",
            form: 'cross'
        }
    }
    if (access == Measures.DB_ACCESS_ORM) {
        return {
            label: 'ORM',
            color: "rgb(255, 205, 86)",
            form: 'circle'
        }
    }
    return undefined;
};

MeasuresHtml.prototype.queriesChangeHandler = function () {
    var $this = this;
    $(".chart-queries-select select").on('change', function (e) {
        $this.replaceQueriesTableHtml()
    });
};

MeasuresHtml.prototype.cpuBaseChartChangeHandler = function () {
    var $this = this;
    $(".chart-cpu-select select").on('change', function (e) {
        $this.clearCpuChart();
        setTimeout($this.cpuBaseChartRender());
    });
};

MeasuresHtml.prototype.memoryBaseChartChangeHandler = function () {
    var $this = this;
    $(".chart-memory-select select").on('change', function (e) {
        $this.clearMemoryBaseChart();
        setTimeout($this.memoryBaseChartRender(), 0);
    });
};

MeasuresHtml.prototype.clearCpuChart = function () {
    var cpuChart = $("#cpuChart");
    $(this.cpuChartHtml()).insertAfter(cpuChart);
    cpuChart.remove();
};

MeasuresHtml.prototype.clearMemoryBaseChart = function () {
    var memoryChart = $("#memoryChart");
    $(this.memoryChartHtml()).insertAfter(memoryChart);
    memoryChart.remove();
};

MeasuresHtml.prototype.cpuBaseChartRender = function () {
    var paramSelect = this.cpuSelect();
    var ctx = document.getElementById('cpuChart').getContext('2d');
    this.baseChartRender(ctx, paramSelect, this.measures.getCpuScaleTime(), this.measures.getCpuScaleCpu());
};

MeasuresHtml.prototype.memoryScatterChartRender = function () {
    var ctx = document.getElementById('memoryScatterChart').getContext('2d');
    var data = this.measures.scatterData(Measures.RESOURCE_MEMORY);
    this.scatterChartRender(ctx, data, this.measures.getMemoryScaleTime(), this.measures.getMemoryScaleMemory());
};

MeasuresHtml.prototype.scatterChartRender = function (ctx, data, xLabelString, yLabelString) {
    var $this = this;
    var datasets = data.map(function (v) {
        return {
            label: $this.chartStyle(v.access).label,
            data: v.data,
            backgroundColor: $this.chartStyle(v.access).color,
            borderColor: $this.chartStyle(v.access).color,
            pointStyle: $this.chartStyle(v.access).form,
            borderWidth: 1,
            fill: false
        };
    });
    this.memoryChart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: datasets
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    scaleLabel: {
                        display: true,
                        labelString: xLabelString
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: yLabelString
                    }
                }]
            }
        }
    });
};

MeasuresHtml.prototype.memoryBaseChartRender = function () {
    var paramSelect = this.memorySelect();
    var ctx = document.getElementById('memoryChart').getContext('2d');
    this.baseChartRender(ctx, paramSelect, this.measures.getMemoryScaleTime(), this.measures.getMemoryScaleMemory());
};

MeasuresHtml.prototype.baseChartRender = function (ctx, paramSelect, xLabelString, yLabelString) {
    var $this = this;
    var datasets = paramSelect.filter(function (v) {
        return !$this.measures.hasError(v.access);
    }).map(function (v) {
        return {
            label: $this.chartStyle(v.access).label,
            data: $this.measures.dataMedian(v.access, v.sort, v.resource, v.statistic),
            backgroundColor: $this.chartStyle(v.access).color,
            borderColor: $this.chartStyle(v.access).color,
            pointStyle: $this.chartStyle(v.access).form,
            borderWidth: 1,
            fill: false
        };
    });
    this.memoryChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: datasets
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    scaleLabel: {
                        display: true,
                        labelString: xLabelString
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: yLabelString
                    }
                }]
            }
        }
    });
};

MeasuresHtml.prototype.cpuChartHtml = function () {
    var html = '';

    html += '<div class="uk-margin-top" style="width:100%;"><canvas id="cpuChart"></canvas></div>';

    return html;
};

MeasuresHtml.prototype.memoryScatterChartHtml = function () {
    var html = '';

    html += '<div class="uk-margin-top" style="width:100%;"><canvas id="memoryScatterChart"></canvas></div>';

    return html;
};

MeasuresHtml.prototype.memoryChartHtml = function () {
    var html = '';

    html += '<div class="uk-margin-top chart-content"><i class="uk-icon-download uk-display-block download download-data-memory-base"></i><canvas id="memoryChart"></canvas></div>';

    return html;
};

MeasuresHtml.prototype.getCpuChartSelectData = function () {
    var selectItem = [
        ['Мінімальна завантаженість процесора', Measures.SORT_RESOURCE, Measures.STATISTIC_MIN],
        ['Медіана завантаженості процесора', Measures.SORT_RESOURCE, Measures.STATISTIC_MEDIAN],
        ['Максимум завантаженості процесора', Measures.SORT_RESOURCE, Measures.STATISTIC_MAX]
    ];
    return this.getChartSelectData(selectItem, 'chart-cpu-select');
};

MeasuresHtml.prototype.getMemoryChartSelectData = function () {
    var selectItem = [
        ['Мінімальний обсяг оперативної пам\'яті', Measures.SORT_RESOURCE, Measures.STATISTIC_MIN],
        ['Медіана обсягу оперативної пам\'яті', Measures.SORT_RESOURCE, Measures.STATISTIC_MEDIAN],
        ['Максимальний обсяг оперативної пам\'яті', Measures.SORT_RESOURCE, Measures.STATISTIC_MAX]
    ];
    return this.getChartSelectData(selectItem, 'chart-memory-select');
};

MeasuresHtml.prototype.getChartSelectData = function (selectItem, elemClass) {
    var html = '';

    var dbAccess = [
        ['Sql', Measures.DB_ACCESS_SQL],
        ['Збережена процедура', Measures.DB_ACCESS_PROC],
        ['Будівальник запитів', Measures.DB_ACCESS_BUILDER],
        ['ORM', Measures.DB_ACCESS_ORM]
    ];
    var timeSelectItem = [
        ['Мінімальний час виконання', Measures.SORT_TIME, Measures.STATISTIC_MIN],
        ['Медіана часу виконання', Measures.SORT_TIME, Measures.STATISTIC_MEDIAN],
        ['Максимальний час виконання', Measures.SORT_TIME, Measures.STATISTIC_MAX]
    ];

    timeSelectItem = timeSelectItem.concat(selectItem);

    var funcSelectHtml = function (typeDbAccess) {
        return (
            '<select>'
                + timeSelectItem.map(function (v, i) {
                    return '<option value="' + i + '"' + (v[1] == Measures.SORT_RESOURCE && v[2] == Measures.STATISTIC_MEDIAN ? 'selected' : '') + ' data-access="' + typeDbAccess + '" data-sort="' + v[1] + '" data-statistic="' + v[2] + '">' + v[0] + '</option>';
                }).join('')
            + '</select>'
        );
    };

    dbAccess.forEach(function (v) {
         html +=
            '<div class="uk-width-1-4 db-access-' + v[1] + '">'
                + '<div>'
                    + v[0]
                + '</div>'
                + '<div>'
                    + funcSelectHtml(v[1])
                + '</div>'
            + '</div>';
    });

    html =
        '<div class="uk-width-1-1 uk-margin-large-top uk-form ' + elemClass + '">'
            + '<div class="uk-grid">'
                + html
            + '</div>'
        + '</div>';

    return html;
};

MeasuresHtml.prototype.queriesSelect = function () {
    var select = $(".chart-queries-select select");
    if (!select || !select.length) {
        return Measures.DB_ACCESS_SQL;
    }
    return select.val();
};

MeasuresHtml.prototype.replaceQueriesTableHtml = function () {
    var queriesTable = $(".queries-table");
    $(this.getQueriesTableHtml()).insertAfter(queriesTable);
    queriesTable.remove();
};

MeasuresHtml.prototype.getQueriesTableHtml = function () {
    var html = '';

    var head =
        '<div class="table__head uk-margin-top">'
            + '<div class="uk-grid">'
                + '<div class="uk-width-medium-1-10">'
                    + '№'
                + '</div>'
                + '<div class="uk-width-medium-9-10">'
                    + 'Запит'
                + '</div>'
            + '</div>'
        + '</div>';

    var content = '';
    var queries = this.measures.getQueries(this.queriesSelect());
    if (!queries || !queries.length) {
        content =
            '<div class="uk-width-medium-1-1">'
                + 'Запити відсутні'
            + '</div>';
    } else {
        queries.forEach(function (v) {
            content +=
                '<div class="uk-width-medium-1-10">'
                    + v.id
                + '</div>'
                + '<div class="uk-width-medium-9-10">'
                    + v.query
                + '</div>';
        })
    }
    content =
        '<div class="table__main uk-margin-top">'
            + '<div class="uk-grid">'
                + content
            + '</div>'
        + '</div>';

    html =
        '<div class="queries-table">'
            + head
            + content
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getSessionStatHtml = function () {
    var html = '';

    var head = this.getTableHeadHtml();
    var measures = this.measures;

    var rows = function (title, prop) {
        var html = '';
        html +=
            '<div class="uk-width-medium-2-10">'
                + title
            + '</div>';
        [
            Measures.DB_ACCESS_SQL,
            Measures.DB_ACCESS_PROC,
            Measures.DB_ACCESS_BUILDER,
            Measures.DB_ACCESS_ORM
        ].forEach(function (v) {
            html +=
                '<div class="uk-width-medium-2-10">'
                    + (
                        measures.hasError(v)
                            ? '-'
                            : (measures.getSessionStat(v)[prop][0] + ' ' + measures.getSessionStat(v)[prop][2])
                    )
                + '</div>';
        });

        html =
            '<div class="table__main">'
                + '<div class="uk-grid">'
                    + html
                + '</div>'
            + '</div>';

        return html;
    };

    html =
        head
        + rows('Даних відправлено', 'byteReceived')
        + rows('Даних прийнято', 'byteSent')
        + rows('Кількість відправлених запитів', 'questions');


    return html;
};

MeasuresHtml.prototype.getQueriesHtml = function () {
    var html = '';
    var $this = this;
    var options = [
        Measures.DB_ACCESS_SQL,
        Measures.DB_ACCESS_PROC,
        Measures.DB_ACCESS_BUILDER,
        Measures.DB_ACCESS_ORM
    ].map(function (v) {
        return '<option value="' + v + '">' + $this.chartStyle(v).label + '</option>';
    });

    html +=
        '<div class="uk-width-1-1 uk-margin-large-top uk-form">'
            + '<div class="chart-queries-select">'
                + 'Доступ за допомогою: '
                + '<select>' + options + '</select>'
            + '</div>'
            + this.getQueriesTableHtml()
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getTableHtml = function () {
    var html = '';

    html +=
        '<div class="uk-width-1-1 table">'
            + this.getTableHeadHtml()
            + this.getTableHeadRowHtml()
            + this.getTableTimeHtml()
            + this.getTableMemoryHtml()
            + (this.measures.hasAnyCpuStamp() ? this.getTableCpuHtml() : '')
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getTableCpuHtml = function () {
    var data = [
        ['Завантаженість процесора, максимум', this.measures.getMaxCpu()],
        ['Завантаженість процесора, у середньому', this.measures.getAvgCpu()],
        ['Завантаженість процесора, мінімум', this.measures.getMinCpu()]
    ];

    //return this.getTableMainHtml(data, this.measures.getCpuScaleCpu());
    return this.getTableMainRowHtml(data, this.measures.getCpuScaleCpu(), 'Завантаженість процесора');
};

MeasuresHtml.prototype.getTableMemoryHtml = function () {
    var data = [
        ['Оперативна пам\'ять, максимум', this.measures.getMaxMemory()],
        ['Оперативна пам\'ять, у середньому', this.measures.getAvgMemory()],
        ['Оперативна пам\'ять, мінімум', this.measures.getMinMemory()]
    ];

    //return this.getTableMainHtml(data, this.measures.getMemoryScaleMemory());
    return this.getTableMainRowHtml(data, this.measures.getMemoryScaleMemory(), 'Оперативна пам\'ять');
};

MeasuresHtml.prototype.getTableTimeHtml = function () {
    var data = [
        ['Час виконання, максимум', this.measures.getMaxTime()],
        ['Час виконання, у середньому', this.measures.getAvgTime()],
        ['Час виконання, мінімум', this.measures.getMinTime()],
        ['Час виконання, загалом', this.measures.getTotalTime()]
    ];

    //return this.getTableMainHtml(data, this.measures.getTimeScale());
    return this.getTableMainRowHtml(data, this.measures.getTimeScale(), 'Час виконання');
};

MeasuresHtml.prototype.getTableMainRowHtml = function (data, scale, title) {
    var html = '';
    var measures = this.measures;

    html +=
        '<div class="table__main">'
            + '<div class="uk-grid">'
                + '<div class="uk-width-medium-2-10">'
                    + title
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + '<div class="uk-grid">'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.sqlHasError() ? '-' : (data[0][1].sql + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.sqlHasError() ? '-' : (data[1][1].sql + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.sqlHasError() ? '-' : (data[2][1].sql + ' ' + scale))
                        + '</div>'
                    + '</div>'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + '<div class="uk-grid">'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.procHasError() ? '-' : (data[0][1].proc + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.procHasError() ? '-' : (data[1][1].proc + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.procHasError() ? '-' : (data[2][1].proc + ' ' + scale))
                        + '</div>'
                    + '</div>'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + '<div class="uk-grid">'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.builderHasError() ? '-' : (data[0][1].builder + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.builderHasError() ? '-' : (data[1][1].builder + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.builderHasError() ? '-' : (data[2][1].builder + ' ' + scale))
                        + '</div>'
                    + '</div>'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + '<div class="uk-grid">'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.ormHasError() ? '-' : (data[0][1].orm + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.ormHasError() ? '-' : (data[1][1].orm + ' ' + scale))
                        + '</div>'
                        + '<div class="uk-width-medium-1-3">'
                            + (measures.ormHasError() ? '-' : (data[2][1].orm + ' ' + scale))
                        + '</div>'
                    + '</div>'
                + '</div>'
            + '</div>'
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getTableMainHtml = function (data, scale) {
    var html = '';

    for(var i = 0; i < data.length; i++) {
        var item = data[i];

        html +=
        '<div class="table__main">'
            + '<div class="uk-grid">'
                + '<div class="uk-width-medium-2-10">'
                    + item[0]
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + item[1].sql + ' ' + scale
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + item[1].proc + ' ' + scale
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + item[1].builder + ' ' + scale
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + item[1].orm + ' ' + scale
                + '</div>'
            + '</div>'
        + '</div>';
    }

    return html;
};

MeasuresHtml.prototype.getTableHeadRowHtml = function () {
    var html = '';

    var minMax =
        '<div class="uk-grid">'
            + '<div class="uk-width-medium-1-3">'
                + 'Макс.'
            + '</div>'
            + '<div class="uk-width-medium-1-3">'
                + 'Сер.'
            + '</div>'
            + '<div class="uk-width-medium-1-3">'
                + 'Мін.'
            + '</div>'
        + '</div>';

    html = '<div class="table__head">'
            + '<div class="uk-grid">'
                + '<div class="uk-width-medium-2-10">'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + minMax
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + minMax
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + minMax
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + minMax
                + '</div>'
            + '</div>'
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getTableHeadHtml = function () {
    var html = '';

    html = '<div class="table__head">'
            + '<div class="uk-grid">'
                + '<div class="uk-width-medium-2-10">'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + 'SQL'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + 'Збережена процедура'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + 'Будівальник запитів'
                + '</div>'
                + '<div class="uk-width-medium-2-10">'
                    + 'ORM'
                + '</div>'
            + '</div>'
        + '</div>';

    return html;
};

MeasuresHtml.prototype.getMapProgress = function () {
    return {
        'sql': '.sql-progress',
        'proc': '.proc-progress',
        'builder': '.builder-progress',
        'orm': '.orm-progress'
    };
};

MeasuresHtml.prototype.progress = function (url, curCount, totalCount) {
    var mapElem = this.getMapProgress();
    var elem = $(mapElem[url]);

    if (elem.length == 0) return;

    elem.css('width', Math.round(curCount / totalCount * 100) + '%');
    elem.text('' + curCount + '/' + totalCount);
};

MeasuresHtml.prototype.clearProgress = function () {
    Object.values(this.getMapProgress()).forEach(function (v) {
        $(v).css('width', 0).text('');
    });
};