'use strict';

$(function () {
    var measuresHtml = new MeasuresHtml();

    $(".code__title").on('click', function ($e) {
        var codeElem = $(this).closest(".code");
        if (codeElem.hasClass('code_open')) {
            codeElem.removeClass('code_open');
        } else {
            codeElem.addClass('code_open');
        }
    });

    $("#send").on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();

        var measures = new Measures();
        measures.load(count, {}, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});