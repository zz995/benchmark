'use strict';

$(function () {
    var measuresHtml = new CommonMeasuresHtml([
        ['records_main', 'Кількість записів (головна таблиця)'],
        ['records_second', 'Кількість записів (залежна таблиця)'],
        ['tables_count', 'Кількість таблиць']
    ]);

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        
        var tablesCount = getActiveParam('tables_count');
        var recordsMain = getActiveParam('records_main');
        var recordsSecond = getActiveParam('records_second');

        var params = [
            {key: 'tables_count', values: tablesCount},
            {key: 'records_main', values: recordsMain},
            {key: 'records_second', values: recordsSecond}
        ];
        var measures = new CommonMeasures(params);
        measures.load(count, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});