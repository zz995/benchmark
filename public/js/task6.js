'use strict';

$(function () {
    var measuresHtml = new MeasuresHtml();

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        var tablesCount = +$("[name='tables_count']").val();
        var recordsMain = +$("[name='records_main']").val();
        var recordsSecond = +$("[name='records_second']").val();

        var measures = new Measures();
        measures.load(count, {
            'tables_count': tablesCount,
            'records_main': recordsMain,
            'records_second': recordsSecond
        }, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});