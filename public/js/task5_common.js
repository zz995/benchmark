'use strict';

$(function () {
    var measuresHtml = new CommonMeasuresHtml([['count', 'Приєднано таблиць'], ['records', 'Кількість записів']]);

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        
        var countTable = getActiveParam('countTable');
        var records = getActiveParam('records');

        var params = [
            {key: 'count', values: countTable},
            {key: 'records', values: records}
        ];
        var measures = new CommonMeasures(params);
        measures.load(count, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});