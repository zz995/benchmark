'use strict';

$(function () {
    $(".params .param a").on('click', function (e) {
        var paramElem = $(this).closest('.param');
        var paramsElem = $(this).closest('.params');
        var activeElem = paramsElem.find(".param.uk-active");

        var name = paramElem.data('name');
        var value = paramElem.data('value');
        var index = paramElem.data('index');

        if (activeElem.length > 1) {
            activeElem.removeClass('uk-active');
        } else {
            var activeIndex = activeElem.eq(0).data('index');
            [].forEach.call(paramsElem.find(".param"), function (elem) {
                elem = $(elem);
                if (
                    (elem.data('index') > index && elem.data('index') < activeIndex)
                    || (elem.data('index') < index && elem.data('index') > activeIndex)
                ) {
                    elem.addClass('uk-active');
                }
            });
        }
        paramElem.addClass('uk-active');

        return false;
    });
});

function getActiveParam(name) {
    return [].map.call($(".params .param.uk-active[data-name='" + name + "']"), function (elem) {
        return $(elem).data('value');
    });
}

function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}