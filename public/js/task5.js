'use strict';

$(function () {
    var measuresHtml = new MeasuresHtml();

    $("#send").off().on('click', function ($e) {
        measuresHtml.startLoad();

        var count = +$("[name='count']").val();
        var tableCount = +$("[name='table']").val();
        var records = +$("[name='records']").val();

        var measures = new Measures();
        measures.load(count, {
            'count': tableCount,
            'records': records
        }, function () {
            measuresHtml.set(measures);
        }, measuresHtml.progress.bind(measuresHtml));
    });
});