<?php

require_once(dirname(__FILE__)."/src/Logger.php");
require_once(dirname(__FILE__)."/src/LoggerStream.php");
require_once(dirname(__FILE__)."/src/FieldBase.php");
require_once(dirname(__FILE__)."/src/TableBase.php");
require_once(dirname(__FILE__)."/src/Table.php");
require_once(dirname(__FILE__)."/src/NumberField.php");
require_once(dirname(__FILE__)."/src/SchemaBuilderBase.php");
require_once(dirname(__FILE__)."/src/SchemaBuilder.php");
require_once(dirname(__FILE__)."/tasks/TaskSchema.php");
require_once(dirname(__FILE__)."/tasks/Task1Schema.php");
require_once(dirname(__FILE__)."/tasks/Task2Schema.php");
require_once(dirname(__FILE__)."/tasks/Task3Schema.php");
require_once(dirname(__FILE__)."/tasks/Task4Schema.php");
require_once(dirname(__FILE__)."/tasks/Task5Schema.php");
require_once(dirname(__FILE__)."/tasks/Task6Schema.php");
