<?php

abstract class TaskSchema {

    protected $builder = null;

    public function __construct(SchemaBuilderBase $builder) {
        $this->builder = $builder;
    }

    public abstract function createSchema();
}