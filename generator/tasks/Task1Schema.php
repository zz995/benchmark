<?php

class Task1Schema extends TaskSchema {

    public function __construct(SchemaBuilderBase $builder) {
        parent::__construct($builder);
    }

    public function createSchema() {
        $this->builder->buildSchema('sc1');
        $this->builder->buildTable('table1', [], 30000);
        $this->builder->addNumberField('table1', 'field1', 1, 100);
        $this->builder->addNumberField('table1', 'field2', 1, 100);
        $this->builder->addNumberField('table1', 'field3', 1, 100);
    }
}