<?php

class Task4Schema extends TaskSchema {

    public function __construct(SchemaBuilderBase $builder) {
        parent::__construct($builder);
    }

    public function createSchema() {
        $this->builder->buildSchema('sc4');
        $this->builder->buildTable('table1_sql', [], 10000);
        $this->builder->addNumberField('table1_sql', 'field1', 1, 100);
        $this->builder->buildTable('table1_proc', [], 10000);
        $this->builder->addNumberField('table1_proc', 'field1', 1, 100);
        $this->builder->buildTable('table1_builder', [], 10000);
        $this->builder->addNumberField('table1_builder', 'field1', 1, 100);
        $this->builder->buildTable('table1_orm', [], 10000);
        $this->builder->addNumberField('table1_orm', 'field1', 1, 100);
    }
}