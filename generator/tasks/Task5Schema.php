<?php

class Task5Schema extends TaskSchema {

    protected $countSecondaryTable = 15;

    public function __construct(SchemaBuilderBase $builder) {
        parent::__construct($builder);
    }

    public function createSchema() {
        $countSecondaryTable = $this->countSecondaryTable;

        $this->builder->buildSchema('sc5');
        $this->builder->buildTable('table1', [], 1000);
        $this->builder->addNumberField('table1', 'field1', 1, 100);
        $this->builder->addNumberField('table1', 'field2', 1, 100);

        for($tableNumber = $countSecondaryTable; $tableNumber--;) {
            $tableName = "table" . ($tableNumber + 2);
            $this->builder->buildTable($tableName, ['table1'], 0);
            $this->builder->bindForegnTable($tableName, 1, 1);
            $this->builder->bindForegnTable($tableName, 2, 5);
            $this->builder->bindForegnTable($tableName, 3, 10);
            $this->builder->bindForegnTable($tableName, 4, 50);
            $this->builder->bindForegnTable($tableName, 5, 100);
            $this->builder->bindForegnTable($tableName, 6, 500);
            $this->builder->addNumberField($tableName, 'field1', 1000, 2000);
        }
        unset($tableNumber);
    }
}