<?php

class Task6Schema extends TaskSchema {

    protected $countSecondaryTable = 15;
    protected $countMainRecords = 200;

    public function __construct(SchemaBuilderBase $builder) {
        parent::__construct($builder);
    }

    public function createSchema() {
        $countSecondaryTable = $this->countSecondaryTable;

        $this->builder->buildSchema('sc6');
        $this->builder->buildTable('table1', [], 30000);
        $this->builder->addNumberField('table1', 'field1', 1, 100);
        $this->builder->addNumberField('table1', 'field2', 1, 100);


        for($tableNumber = $countSecondaryTable; $tableNumber--;) {
            $tableName = "table" . ($tableNumber + 2);
            $this->builder->buildTable($tableName, ['table1'], 0);
            $index = $this->countMainRecords;

            foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] as $countSecondaryRecords) {
                for($countRecords = $this->countMainRecords; $countRecords--;) {
                    $this->builder->bindForegnTable($tableName, ++$index, $countSecondaryRecords);
                }
            }

            $this->builder->addNumberField($tableName, 'field1', 1000, 2000);
        }
        unset($tableNumber);
    }
}