<?php

require_once(dirname(__FILE__) . "/common.inc.php");
$config = require_once(dirname(__FILE__) . "/config.php");
$logger = new LoggerStream();

set_exception_handler(function ($exception) use($logger) {
    $logger->error($exception);
    return null;
});

$pdo = new PDO(
    "mysql:host={$config['database']['host']};dbname={$config['database']['dbname']};charset={$config['database']['charset']}",
    $config['database']['username'],
    $config['database']['password'],
    [
        PDO::ATTR_CASE => PDO::CASE_LOWER,
        PDO::ATTR_PERSISTENT => TRUE,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NUM,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false
    ]
);

$schemaBuilder = new SchemaBuilder($pdo);
//$taskSchema = new Task1Schema($schemaBuilder);
//$taskSchema = new Task2Schema($schemaBuilder);
//$taskSchema = new Task3Schema($schemaBuilder);
//$taskSchema = new Task4Schema($schemaBuilder);
//$taskSchema = new Task5Schema($schemaBuilder);
//$taskSchema = new Task6Schema($schemaBuilder);
$taskSchema->createSchema();
$schemaBuilder->commit();
