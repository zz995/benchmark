<?php

abstract class SchemaBuilderBase {
    protected $sql = '';
    protected $deleteOldSchema = 1;
    protected $pdo;
    protected $schema;
    protected $tables = [];

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function buildSchema(string $name) {
        $this->schema = $name;
    }

    public function bindForegnTable($name, $id, $count) {
        if (!isset($this->tables[$name])) {
            throw new \Exception('Table not exist');
        }

        $this->tables[$name]->addBind([$count, $id]);
    }

    public function buildTable(string $name, array $parents = [], int $countRows) {
        if (is_null($this->schema)) {
            throw new \Exception('Schema not defined');
        }
        if (isset($this->tables[$name])) {
            throw new \Exception('Table already added');
        }

        $table = new Table($this->pdo, empty($this->schema) ? $name : ($this->schema . '_' . $name));
        $table->setCountRows($countRows);
        foreach ($parents as $parent) {
            if (!isset($this->tables[$parent])) continue;

            $table->bindTo($this->tables[$parent]);
        }

        $this->tables[$name] = $table;
    }

    public function addNumberField(string $table, string $field, int $first, int $last) {
        if (!isset($this->tables[$table])) {
            throw new \Exception('Table not defined');
        }

        $this->tables[$table]->addField(new NumberField($field, $first, $last));
    }



    public function commit() {
        foreach ($this->tables as $table) {
            $table->commit();
        }
    }
}