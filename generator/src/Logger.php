<?php

abstract class Logger {
    const ERROR = 'ERROR';
    const WARNING = 'WARNING';
    const NOTICE = 'NOTICE';

    protected $writeStackTrace = false;

    public function __construct(bool $stackTrace) {
        $this->writeStackTrace = $stackTrace;
    }

    public function error(\Exception $exception) {
        $msg = $exception->getMessage() . ' : ' .  $exception->getFile() . ':' .  $exception->getLine();
        if ($this->writeStackTrace) {
            $msg .= PHP_EOL . $exception->getTraceAsString();
        }

        $this->printString(static::ERROR, $msg);
    }

    protected abstract function printString($type, $msg);
}