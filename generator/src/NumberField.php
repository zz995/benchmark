<?php

class NumberField extends FieldBase {
    protected $first = 1;
    protected $last = 1;

    public function __construct(string $name, int $first, int $last) {
        parent::__construct($name);
        $this->first = $first;
        $this->last = $last;
    }

    public function __toString() {
        return "`{$this->name}` INT NOT NULL";
    }

    public function getValue(): int {
        return rand($this->first, $this->last);
    }
}