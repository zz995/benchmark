<?php

abstract class FieldBase {
    protected $name;
    public function __construct(string $name) {
        $this->name = $name;
    }

    public abstract function __toString();

    public abstract function getValue();

    public function getName() {
        return $this->name;
    }
}