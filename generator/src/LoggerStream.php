<?php

class LoggerStream extends Logger {
    public function __construct($stackTrace = 1) {
        parent::__construct($stackTrace);
    }

    public function printString($type, $msg) {
        echo static::ERROR . ' ' . $msg;
    }
}