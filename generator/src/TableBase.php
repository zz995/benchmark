<?php

abstract class TableBase {
    protected $pdo;
    protected $name;
    protected $totalRows;
    protected $fields = [];
    protected $parents = [];
    protected $bind = [];
    protected $currentId = 1;

    public function __construct(PDO $pdo, string $name) {
        $this->pdo = $pdo;
        $this->name = $name;
    }

    public function setCountRows(int $count) {
        $this->totalRows = $count;
    }

    public function addField(FieldBase $field) {
        $this->fields[] = $field;
    }

    public function getTotalRow(): int {
        return $this->totalRows;
    }

    public function getRandId(): int {
        return rand(1, $this->totalRows);
    }

    public function addBind(array $arr) {
        $this->bind[] = $arr;
    }

    public function bindTo(TableBase $table) {
        $this->parents[] = $table;
    }

    public function commit() {
        $this->createTable();
        $this->insertValues();
    }

    protected function createTable() {
        $fields = $this->fields;
        foreach ($this->parents as $table) {
            $fields[] = "`{$table->name}_id` INT NOT NULL";
        }
        $sqlFields = implode(',', $fields);
        if (count($this->fields) > 0) {
            $sqlFields .= ',';
        }

        $currentName = $this->name;
        $sqlForeign = implode(',', array_map(function ($t)use($currentName) { return $t->foreignSql($currentName);}, $this->parents));
        if (count($this->parents) > 0) {
            $sqlForeign = ',' . $sqlForeign;
        }

        $sql = "
            CREATE TABLE `{$this->name}` (
                `id` INT NOT NULL,
                $sqlFields
                PRIMARY KEY (`id`)
                $sqlForeign
            );
        ";

        $this->pdo->query($sql);
    }

    protected function insertValues() {
        $values = '';
        for($i = 0; $i < $this->totalRows; $i++) {
            $values .= $this->getRowValues() . ',';
        }

        if (!$this->totalRows) {
            foreach ($this->bind as $b) {
                for($i = 0; $i < $b[0]; $i++) {
                    $values .= $this->getRowValues($b[1]) . ',';
                }
            }
        }

        $values = substr($values, 0, -1);
        $fieldNames = $this->getRowTitles();
        $sql = "INSERT INTO `{$this->name}` $fieldNames VALUES $values";
        $this->pdo->query($sql);
    }

    protected function foreignSql($name) {
        $sql = "
            INDEX `{$this->name}_id_idx` (id ASC),
            CONSTRAINT `{$this->name}_{$name}_id`
                FOREIGN KEY (`{$this->name}_id`)
                REFERENCES `{$this->name}` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
        ";

        return $sql;
    }

    protected function getRowValues($bindId = null) {
        $values = [];
        $values[] = $this->currentId++;
        foreach ($this->fields as $field) {
            $values[] = $field->getValue();
        }
        foreach ($this->parents as $table) {
            $values[] = is_null($bindId) ? $table->getRandId() : $bindId;
        }
        return '(' . implode(',', $values) . ')';
    }

    protected function getRowTitles() {
        $titles = [];
        $titles[] = 'id';
        foreach ($this->fields as $field) {
            $titles[] = $field->getName();
        }
        foreach ($this->parents as $table) {
            $titles[] = "`{$table->name}_id`";
        }
        return '(' . implode(',', $titles) . ')';
    }
}